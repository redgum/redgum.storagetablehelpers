﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Redgum.StorageTableHelpers
{
    public class OrderedQueueEntity<T> : TableEntity where T : new()
    {
        public OrderedQueueEntity() : base()
        {
            //
        }

        public DateTime VisibleAtUtc { get; set; }
        //LeaseIdentifier can not be nullable because you can't query on Null and we need to know when it's 'empty'
        public Guid LeaseIdentifier { get; set; }
        //LeaseExpiresAt can be nullable because we only need to query on it when it contains information, we never need to query it for 'empty'
        public DateTime? LeaseExpiresAtUtc { get; set; }
        public int DequeueCount { get; set; }

        //The Data property won't be stored directly in the Azure Table Storage, we'll json serialise it in and out
        public T Data { get; set; }
        //this is where we store the json serialised version of the Data property
        public string DataJson { get; set; }
    }

    public class OrderedQueue<T> where T : new()
    {
        CloudTable _table;
        string _queueName;
        TimeSpan _leaseDuration;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="storageAccount">Azure Storage Account</param>
        /// <param name="tableName">Name of the Azure Storage Table</param>
        /// <param name="queueName">queueName will be used as the partition key.</param>
        /// <param name="leaseDuration">time lease will last</param>
        public OrderedQueue(CloudStorageAccount storageAccount, string tableName, string queueName, TimeSpan leaseDuration)
        {
            var tableClient = storageAccount.CreateCloudTableClient();
            _table = tableClient.GetTableReference(tableName);
            _queueName = queueName;
            _leaseDuration = leaseDuration;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="table">Azure Storage Table where queue data will be stored.</param>
        /// <param name="queueName">queueName will be used as the partition key.</param>
        /// <param name="leaseDuration">time lease will last</param>
        public OrderedQueue(CloudTable table, string queueName, TimeSpan leaseDuration)
        {
            _table = table;
            _queueName = queueName;
            _leaseDuration = leaseDuration;
        }
        //LeaseIdentifier can't be nullable because you can't query on Null and we need to know when it's 'empty'
        //so we use a well known value for empty or not leased lease identifiers
        private static Guid NoLeaseIdentifier { get; } = Guid.Empty;

        private string GetRowKey(DateTime visibleAt)
        {
            //using visibleAt as part of our RowKey means that results are inserted in the order that they need to be processed
            //the downside of this is that visibleAt shouldn't be editable from the outside world because we can't alter the RowKey
            //to alter visibleAt, we'd have to delete the item and insert it back in - not an impossible task by any stretch of the imagination, but we don't need it at the moment
            return $"{visibleAt.FormatForTableOrdering()}-{Guid.NewGuid()}";
        }
        public async Task<OrderedQueueEntity<T>> AddItem(T data, DateTime visibleAtUtc)
        {
            var item = new OrderedQueueEntity<T>()
            {
                PartitionKey = _queueName,
                RowKey = GetRowKey(visibleAtUtc),
                VisibleAtUtc = visibleAtUtc,
                LeaseIdentifier = NoLeaseIdentifier,
                LeaseExpiresAtUtc = null,
                DequeueCount = 0,
                //Data = data,
                DataJson = JsonConvert.SerializeObject(data)
            };

            var tableResult = await AddItem(item);
            //we know that the other AddItem will have thrown an exception if something went wrong, so we can direct cast the result and return it
            var result = (OrderedQueueEntity<T>)tableResult.Result;
            //but before we return the result, wire up the same 'data' so that its not confusing for the caller that .data is null 
            result.Data = data;
            return result;
        }
        private async Task<TableResult> AddItem(OrderedQueueEntity<T> item)
        {
            var operation = TableOperation.Insert(item);
            //this will throw an exception if the insert fails
            return await _table.ExecuteAsync(operation);
        }

        public async Task<OrderedQueueEntity<T>> GetNextItem()
        {
            var currentDate = DateTime.UtcNow;

            var peekResult = await PeekNextItem();
            //no visible items in this queue, return null and we're done here
            if (peekResult == null) return null;

            //see if we can obtain a lease
            var leaseResult = await ObtainLease(peekResult);
            if (leaseResult == null)
            {
                //couldn't get a lease on that one, someone must have beat us to it
                //recurse to see if there are any more items in the queue
                return await GetNextItem();
            }
            else
            {
                //we win, return the successfully leased entity
                var result = leaseResult;
                result.Data = JsonConvert.DeserializeObject<T>(result.DataJson);
                return result;
            }
        }

        //Used for thread locking while we obtain a lease
        static SemaphoreSlim _semaphoreSlimForLease = new SemaphoreSlim(1, 1);

        //this is only internal to allow for testing, it really should be private
        internal async Task<OrderedQueueEntity<T>> ObtainLease(OrderedQueueEntity<T> item)
        {
            //some guards
            if (!Guid.Equals(item.LeaseIdentifier, NoLeaseIdentifier))
            {
                //item already has a lease
                return null;
            }
            if (item.LeaseExpiresAtUtc.HasValue)
            {
                //item already has a lease
                //perhaps should throw an exception because it has a LeaseExpiresAt but not a LeaseIdentifier
                //but I guess concurrency could cause that
                return null;
            }


            var currentDate = DateTime.UtcNow;

            //we've got an item to work with, lets try to get a lease on it
            var operation = TableOperation.Merge(item);

            //-----------------------------------------------------------------------------------------
            // Locking
            //-----------------------------------------------------------------------------------------
            //for doing locking with SemaphoreSlim
            //see: https://blog.cdemi.io/async-waiting-inside-c-sharp-locks/
            // https://stackoverflow.com/a/32085062/6042
            //if we don't like SemaphoreSlim we could use https://github.com/StephenCleary/AsyncEx 

            //Asynchronously wait to enter the Semaphore. If no-one has been granted access to the Semaphore, 
            //code execution will proceed, otherwise this thread waits here until the semaphore is released 
            await _semaphoreSlimForLease.WaitAsync();
            //we try to keep activity within the lock to a bare minimum 
            //but we do have to be locked for the entirety of the call that actually obtains the lease (through a Merge), 
            //that's the whole point

            try
            {
                //check again that the item hasn't been leased while we waited for Texas Slim to do his thing,
                //      according to him "threads will block while acquiring the lock" upon a rock on the dock in a sock with a glock
                //Make sure we don't have a lease identifier or a LeaseExpiresAt already
                if (!Guid.Equals(item.LeaseIdentifier, NoLeaseIdentifier))
                {
                    //item already has a lease
                    return null;
                }
                if (item.LeaseExpiresAtUtc.HasValue)
                {
                    //item already has a lease
                    //perhaps should throw an exception because it has a LeaseExpiresAt but not a LeaseIdentifier
                    return null;
                }

                //we're free and clear to try to get a lock on this item - thats from the perspective this application
                //other systems might be accessing the same item, but eTags will help us with those collision issues 
                //update the values that Lease cares about
                item.LeaseIdentifier = Guid.NewGuid();
                item.LeaseExpiresAtUtc = currentDate.Add(_leaseDuration);
                item.DequeueCount++;

                //we got our operation organised before we took the lock, so now we're free to execute it
                //attempt to acquire the lease 
                var updateResult = await _table.ExecuteAsync(operation);
                return (OrderedQueueEntity<T>)updateResult.Result;
                //I was checking updateResult.HttpStatusCode here, but it's unnecessary
                //it'll throw an exception for everything except the 200 range
            }
            catch (StorageException ex)
            {
                if (ex.RequestInformation.HttpStatusCode == (int)HttpStatusCode.PreconditionFailed)
                {
                    //someone else, probably on a different system, already taken a lease on that item
                    //the underlying library has utilised eTags to tell us that the items was modified on the server so we're sh!t outta luck
                    //return null to indicate that getting the lease failed
                    return null;
                }
                else
                {
                    throw;
                }
            }
            finally
            {
                //When the task is ready, release the semaphore. It is vital to ALWAYS release the semaphore when we are ready, or else we will end up with a Semaphore that is forever locked.
                //This is why it is important to do the Release within a try...finally clause; program execution may crash or take a different path, this way you are guaranteed execution
                _semaphoreSlimForLease.Release();
            }
            //end of locking
            //-----------------------------------------------------------------------------------------
        }

        public async Task<OrderedQueueEntity<T>> PeekNextItem()
        {
            var results = await PeekNextItems(itemCount: 1);
            return results.FirstOrDefault();
        }
        public async Task<IList<OrderedQueueEntity<T>>> PeekNextItems(int itemCount)
        {
            var currentDate = DateTime.UtcNow;

            var filter = VisibleAfterFilter(currentDate);
            filter = TableQuery.CombineFilters(
                filter,
                TableOperators.And,
                TableQuery.GenerateFilterConditionForGuid("LeaseIdentifier", QueryComparisons.Equal, NoLeaseIdentifier)
                );

            var query = new TableQuery<OrderedQueueEntity<T>>()
                .Where(filter)
                .Take(itemCount)
                ;

            var result = await _table.ExecuteQueryAsync(query);
            if (result != null)
            {
                foreach (var item in result)
                {
                    item.Data = JsonConvert.DeserializeObject<T>(item.DataJson);
                }
            }
            return result;
        }

        private string PartitionKeyFilter()
        {
            return TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, _queueName);
        }
        private string PartitionKeyAndRowKeyFilter(DateTime date)
        {
            var dateString = date.FormatForTableOrdering();
            return TableQuery.CombineFilters(
                PartitionKeyFilter(),
                TableOperators.And,
                TableQuery.GenerateFilterCondition("RowKey ", QueryComparisons.LessThanOrEqual, dateString)
                );
        }
        private string VisibleAfterFilter(DateTime date)
        {
            //use the RowKey as part of our lookup
            return TableQuery.CombineFilters(
                PartitionKeyAndRowKeyFilter(date),
                TableOperators.And,
                TableQuery.GenerateFilterConditionForDate("VisibleAtUtc", QueryComparisons.LessThanOrEqual, date)
                );
        }

        public async Task DeleteItem(OrderedQueueEntity<T> item)
        {
            var operation = TableOperation.Delete(item);
            var deleteResult = await _table.ExecuteAsync(operation);
        }

        /// <summary>
        /// Note that this can be an expensive operation because it requires pulling part of every row back and then doing a .Count() on the list returned.
        /// </summary>
        /// <returns></returns>
        public async Task<int> ApproximateVisibleQueueLength()
        {
            var currentDate = DateTime.UtcNow;
            var filter = VisibleAfterFilter(currentDate);

            //the only way to get a row count is to return all the rows we care about and do a .Count()
            //we don't have to return the entire row though, we only have to return the bare minimum, which is "PartitionKey", "RowKey", "Timestamp" 
            TableQuery<TableEntity> query = new TableQuery<TableEntity>()
                .Where(filter)
                .Select(new List<string> { "PartitionKey", "RowKey", "Timestamp" });

            var queryResult = await _table.ExecuteQueryAsync(query);
            return queryResult.Count();
        }

        /// <summary>
        /// Note that this is an expensive operation because it requires pulling part of every row back and then doing a .Count() on the list returned.
        /// </summary>
        /// <returns></returns>
        public async Task<int> ApproximateTotalQueueLength()
        {
            var filter = PartitionKeyFilter();

            //the only way to get a row count is to return all the rows we care about and do a .Count()
            //we don't have to return the entire row though, we only have to return the bare minimum, which is "PartitionKey", "RowKey", "Timestamp" 
            TableQuery<TableEntity> query = new TableQuery<TableEntity>()
                .Where(filter)
                .Select(new List<string> { "PartitionKey", "RowKey", "Timestamp" });

            var queryResult = await _table.ExecuteQueryAsync(query);
            return queryResult.Count();
        }
        /// <summary>
        /// Ping this at regular intervals to ensure that all leases are times out accordingly. 
        /// This functionality could potentially be called everytime something is Peeked, but until we understand how heavy this call is it's better to have it completely separated
        /// </summary>
        /// <param name="batchSize">Max potential number of items that will be timed out at once if required</param>
        /// <returns></returns>
        public async Task<IEnumerable<OrderedQueueEntity<T>>> TimeOutExpiredLeases(int batchSize)
        {
            var currentDate = DateTime.UtcNow;

            var filter = TableQuery.CombineFilters(
                PartitionKeyFilter(),
                TableOperators.And,
                TableQuery.GenerateFilterConditionForDate("LeaseExpiresAtUtc", QueryComparisons.LessThanOrEqual, currentDate)
                );

            var query = new TableQuery<OrderedQueueEntity<T>>()
                .Where(filter)
                .Take(batchSize)
                ;

            //get the items that need to have their Leases Expired
            var itemsWithExpiredLeases = await _table.ExecuteQueryAsync(query);
            //Expire the leases on those items
            await TimeOutExpiredLeases(itemsWithExpiredLeases);
            //return those items because caller might want to do some logging
            return itemsWithExpiredLeases;
        }
        internal async Task TimeOutExpiredLeases(IEnumerable<OrderedQueueEntity<T>> itemsToExpire)
        {
            foreach (var item in itemsToExpire)
            {
                await TimeOutExpiredLease(item);
            }
        }
        internal async Task TimeOutExpiredLease(OrderedQueueEntity<T> itemToExpire)
        {
            itemToExpire.LeaseExpiresAtUtc = null;
            itemToExpire.LeaseIdentifier = NoLeaseIdentifier;
            //we have to use Replace instead of Merge, because Merge won't overwrite the dateTime (LeaseExpiresAt) with null, 
            //it'll assume its not one of the fields we're trying to merge
            var operation = TableOperation.Replace(itemToExpire);
            await _table.ExecuteAsync(operation);
        }
    }
}
