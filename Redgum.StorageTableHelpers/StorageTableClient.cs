﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Redgum.StorageTableHelpers
{

    public class TableId
    {
        public string PartitionId { get; set; }
        public string RowKey { get; set; }
    }

    public interface IStorageTableClient
    {
        Task<IReadOnlyList<T>> Get<T>(string partitionKey) where T : ITableEntity, new();
        Task<T> Get<T>(TableId id) where T : ITableEntity, new();
        Task<T> Get<T>(string partitionKey, string rowKey) where T : ITableEntity, new();
        Task<IReadOnlyList<T>> Get<T>(string partitionKey, string rowKeyRangeStart, string rowKeyRangeEnd, bool inclusive) where T : ITableEntity, new();
        Task<int> GetCount(string partitionKey);
        Task<int> GetCount(string partitionKey, string rowKeyRangeStart, string rowKeyRangeEnd, bool inclusive);
        Task<bool> Exists(string partitionKey, string rowkey);
        Task<bool> Exists(string partitionKey);
        Task<TableResult> Insert<T>(T doc) where T : ITableEntity, new();
        Task Upsert<T>(T doc) where T : ITableEntity, new();

        /// <summary>
        /// This does hide some facts from the end developer.
        /// All entities must share a PartitionKey
        /// The maximum number of entities that can be in any one batch is 100, if the list parameter contains more than 100 entities multiple batch operations (or transactions) are performed.
        /// The maximum size of batch (or transaction) is 4MB, if any of the transactions contain more than 4MB of data this method will raise an exception.
        /// </summary>
        /// <typeparam name="T">Anything that implements ITableEntity, that includes TableEntity</typeparam>
        /// <param name="list">A list of objects that implement ITableEntity that all have the same PartitionKey</param>
        /// <returns> A System.Collections.Generic.IList that contains results from ExecuteBatchAsync().</returns>
        Task<IList<TableResult>> InsertBatch<T>(IReadOnlyList<T> list) where T : ITableEntity;
        /// <summary>
        /// This does hide some facts from the end developer.
        /// All entities must share a PartitionKey
        /// The maximum number of entities that can be in any one batch is 100, if the list parameter contains more than 100 entities multiple batch operations (or transactions) are performed.
        /// The maximum size of batch (or transaction) is 4MB, if any of the transactions contain more than 4MB of data this method will raise an exception.
        /// </summary>
        /// <typeparam name="T">Anything that implements ITableEntity, that includes TableEntity</typeparam>
        /// <param name="list">A list of objects that implement ITableEntity that all have the same PartitionKey</param>
        /// <returns> A System.Collections.Generic.IList that contains results from ExecuteBatchAsync().</returns>
        Task<IList<TableResult>> InsertOrReplaceBatch<T>(IEnumerable<T> list) where T : ITableEntity;
        Task<IList<TableResult>> DeleteBatch(string partitionKey, IEnumerable<string> rowKeys);
        Task<TableResult> Delete(string partitionKey, string rowKey);
        Task<IList<TableResult>> Delete(string partitionKey);
        Task<T> Pop<T>(string partitionKey, string rowKey) where T : ITableEntity, new();
        Task<TableResult> InsertJsonData<T>(string partitionKey, string rowKey, T data);
        Task<IList<TableResult>> InsertOrReplaceBatchJsonData<T>(string partitionKey, Func<T, string> rowKeyFun, IReadOnlyList<T> data);
        Task UpsertJsonData<T>(string partitionKey, string rowKey, T data);
        Task<T> GetJsonData<T>(string partitionKey, string rowKey);
        Task<IReadOnlyList<T>> GetJsonData<T>(string partitionKey);

        Task<IList<T>> ExecuteQueryAsync<T>(TableQuery<T> query) where T : ITableEntity, new();
        Task<IList<T>> ExecuteQueryAsync<T>(TableQuery<T> query, CancellationToken ct = default, Action<IList<T>> onProgress = null) where T : ITableEntity, new();
        Task<TableResult> ExecuteAsync(TableOperation operation, TableRequestOptions requestOptions, OperationContext operationContext, CancellationToken cancellationToken);
        Task<TableResult> ExecuteAsync(TableOperation operation, TableRequestOptions requestOptions, OperationContext operationContext);
        Task<TableResult> ExecuteAsync(TableOperation operation);

        Task<TableResult> InsertOrReplaceJsonData<T>(string partitionKey, string rowKey, T data);
        Task<TableResult> InsertOrReplace<T>(T entity) where T : ITableEntity, new();
        Task<TableResult> InsertOrMerge<T>(T entity) where T : ITableEntity, new();
        Task<TableResult> Merge<T>(T entity) where T : ITableEntity, new();
        Task<TableResult> Replace<T>(T entity) where T : ITableEntity, new();
        Task<IReadOnlyList<T>> GetAll<T>() where T : ITableEntity, new();
    }

    /// <summary>
    /// encapsulates an azure cloud table to simplify using it
    /// and make all calls as optimised as possible
    /// </summary>
    public class StorageTableClient : IStorageTableClient
    {
        private readonly CloudTable _table;

        const int maxBatchSize = 100;

        public StorageTableClient(CloudTable table)
        {
            _table = table ?? throw new ArgumentNullException(paramName: nameof(table));
        }

        /// <summary>
        /// gets every record from every partition
        /// this could be an extremely slow call, use wisely!
        /// </summary>
        public async Task<IReadOnlyList<T>> GetAll<T>() where T : ITableEntity, new()
        {
            // TODO make a paged version of this at some point
            var query = new TableQuery<T>();

            var re = new List<T>();
            TableContinuationToken continuationToken = null;
            do
            {
                var r = await _table.ExecuteQuerySegmentedAsync(query, continuationToken);

                re.AddRange(r);
                continuationToken = r.ContinuationToken;
            } while (continuationToken != null);
            return re;
        }

        public async Task<IReadOnlyList<T>> Get<T>(string partitionKey) where T : ITableEntity, new()
        {
            var query = new TableQuery<T>().Where(CloudTableUtils.CreateFilter(partitionKey));

            var re = new List<T>();
            TableContinuationToken continuationToken = null;
            do
            {
                var r = await _table.ExecuteQuerySegmentedAsync(query, continuationToken);

                re.AddRange(r);
                continuationToken = r.ContinuationToken;
            } while (continuationToken != null);
            return re;
        }

        public async Task<IReadOnlyList<T>> Get<T>(string partitionKey, string rowKeyRangeStart, string rowKeyRangeEnd, bool inclusive) where T : ITableEntity, new()
        {
            var filter = CloudTableUtils.CreateRangeFilter(partitionKey, rowKeyRangeStart, rowKeyRangeEnd, inclusive);

            //            var filter = CloudTableUtils.CreateFilter(partitionKey,rowFilter);
            var query = new TableQuery<T>().Where(filter);

            var re = new List<T>();
            TableContinuationToken continuationToken = null;
            do
            {
                var r = await _table.ExecuteQuerySegmentedAsync(query, continuationToken);

                re.AddRange(r);
                continuationToken = r.ContinuationToken;
            } while (continuationToken != null);
            return re;
        }

        public async Task<T> Get<T>(TableId id) where T : ITableEntity, new()
        {
            return await Get<T>(id.PartitionId, id.RowKey);
        }

        public async Task<T> Get<T>(string partitionKey, string rowKey) where T : ITableEntity, new()
        {
            var filter = CloudTableUtils.CreateFilter(partitionKey, rowKey);
            var query = new TableQuery<T>().Where(filter);

            var re = new T();
            TableContinuationToken continuationToken = null;
            do
            {
                var r = await _table.ExecuteQuerySegmentedAsync(query, continuationToken);

                re = r.FirstOrDefault();
                continuationToken = r.ContinuationToken;
            } while (continuationToken != null);

            return re;
        }
        public async Task<IList<T>> ExecuteQueryAsync<T>(TableQuery<T> query) where T : ITableEntity, new()
        {
            return await ExecuteQueryAsync(query, ct: default(CancellationToken), onProgress: null);
        }
        public async Task<IList<T>> ExecuteQueryAsync<T>(TableQuery<T> query, CancellationToken ct = default(CancellationToken), Action<IList<T>> onProgress = null) where T : ITableEntity, new()
        {
            return await _table.ExecuteQueryAsync(query, ct, onProgress);
        }
        public async Task<TableResult> ExecuteQueryAsync(TableOperation operation)
        {
            return await _table.ExecuteAsync(operation);
        }
        public Task<TableResult> ExecuteAsync(TableOperation operation, TableRequestOptions requestOptions, OperationContext operationContext, CancellationToken cancellationToken)
        {
            return _table.ExecuteAsync(operation, requestOptions, operationContext, cancellationToken);
        }
        public Task<TableResult> ExecuteAsync(TableOperation operation, TableRequestOptions requestOptions, OperationContext operationContext)
        {
            return _table.ExecuteAsync(operation, requestOptions, operationContext);
        }
        public Task<TableResult> ExecuteAsync(TableOperation operation)
        {
            return _table.ExecuteAsync(operation);
        }

        public async Task<int> GetCount(string partitionKey)
        {
            var keyFilter = CloudTableUtils.CreateFilter(partitionKey);

            var query = CloudTableUtils.CreateMinimalQuery(keyFilter);

            return await _table.ExecuteCountQuery(query);
        }

        public async Task<int> GetCount(string partitionKey, string rowKeyRangeStart, string rowKeyRangeEnd, bool inclusive)
        {

            var filter = CloudTableUtils.CreateRangeFilter(partitionKey, rowKeyRangeStart, rowKeyRangeEnd, inclusive);

            var query = CloudTableUtils.CreateMinimalQuery(filter);

            return await _table.ExecuteCountQuery(query);
        }

        public async Task<bool> Exists(string partitionKey, string rowkey)
        {
            var filter = CloudTableUtils.CreateFilter(partitionKey, rowkey);

            return await _table.ExecuteExistsQuery(filter);
        }

        public async Task<bool> Exists(string partitionKey)
        {
            var filter = CloudTableUtils.CreateFilter(partitionKey);

            return await _table.ExecuteExistsQuery(filter);
        }

        public async Task<TableResult> Insert<T>(T doc) where T : ITableEntity, new()
        {
            var insertOperation = TableOperation.Insert(doc);
            return await _table.ExecuteAsync(insertOperation);
        }

        [Obsolete("Use InsertOrReplace instead, that's what this really does")]
        public async Task Upsert<T>(T doc) where T : ITableEntity, new()
        {
            var insertOperation = TableOperation.InsertOrReplace(doc);
            var r = await _table.ExecuteAsync(insertOperation);
        }

        public async Task<TableResult> InsertOrReplace<T>(T entity) where T : ITableEntity, new()
        {
            var insertOperation = TableOperation.InsertOrReplace(entity);
            return await _table.ExecuteAsync(insertOperation);
        }
        public Task<TableResult> InsertOrMerge<T>(T entity) where T : ITableEntity, new()
        {
            var insertOperation = TableOperation.InsertOrMerge(entity);
            return _table.ExecuteAsync(insertOperation);
        }
        public Task<TableResult> Merge<T>(T entity) where T : ITableEntity, new()
        {
            var insertOperation = TableOperation.Merge(entity);
            return _table.ExecuteAsync(insertOperation);
        }
        public Task<TableResult> Replace<T>(T entity) where T : ITableEntity, new()
        {
            var insertOperation = TableOperation.Replace(entity);
            return _table.ExecuteAsync(insertOperation);
        }

        /// <summary>
        /// This does hide some facts from the end developer.
        /// All entities must share a PartitionKey
        /// The maximum number of entities that can be in any one batch is 100, if the list parameter contains more than 100 entities multiple batch operations (or transactions) are performed.
        /// The maximum size of batch (or transaction) is 4MB, if any of the transactions contain more than 4MB of data this method will raise an exception.
        /// </summary>
        /// <typeparam name="T">Anything that implements ITableEntity, that includes TableEntity</typeparam>
        /// <param name="list">A list of objects that implement ITableEntity that all have the same PartitionKey</param>
        /// <returns> A System.Collections.Generic.IList that contains results from ExecuteBatchAsync().</returns>
        public async Task<IList<TableResult>> InsertBatch<T>(IReadOnlyList<T> list) where T : ITableEntity
        {
            var result = new List<TableResult>();
            var batches = list.SplitToBatches(maxBatchSize).ToList();

            foreach (var batch in batches)
            {
                var batchOperation = new TableBatchOperation();

                foreach (var doc in batch)
                {
                    batchOperation.Insert(doc);
                }
                var rr = await _table.ExecuteBatchAsync(batchOperation);
                result.AddRange(rr);
            }
            return result;
        }

        /// <summary>
        /// This does hide some facts from the end developer.
        /// All entities must share a PartitionKey
        /// The maximum number of entities that can be in any one batch is 100, if the list parameter contains more than 100 entities multiple batch operations (or transactions) are performed.
        /// The maximum size of batch (or transaction) is 4MB, if any of the transactions contain more than 4MB of data this method will raise an exception.
        /// </summary>
        /// <typeparam name="T">Anything that implements ITableEntity, that includes TableEntity</typeparam>
        /// <param name="list">A list of objects that implement ITableEntity that all have the same PartitionKey</param>
        /// <returns> A System.Collections.Generic.IList that contains results from ExecuteBatchAsync().</returns>
        public async Task<IList<TableResult>> InsertOrReplaceBatch<T>(IEnumerable<T> list) where T : ITableEntity
        {
            var result = new List<TableResult>();
            var batches = list.SplitToBatches(maxBatchSize).ToList();

            foreach (var batch in batches)
            {
                var batchOperation = new TableBatchOperation();

                foreach (var doc in batch)
                {
                    batchOperation.InsertOrReplace(doc);
                }
                var rr = await _table.ExecuteBatchAsync(batchOperation);
                result.AddRange(rr);
            }
            return result;
        }

        public async Task<IList<TableResult>> DeleteBatch(string partitionKey, IEnumerable<string> rowKeys)
        {
            var result = new List<TableResult>();
            // TODO this could probably be made more efficient

            var rowKeysHash = rowKeys.ToDictionary(k => k, v => v);
            var deleteBatches = new Dictionary<int, List<DynamicTableEntity>>();

            var currentBatch = 0;
            deleteBatches[currentBatch] = new List<DynamicTableEntity>();


            // grab everything in this partition (note some might not need deleting)
            var q = new TableQuery().Where(CloudTableUtils.CreateFilter(partitionKey));

            // loop through all results for this partition
            TableContinuationToken continuationToken = null;
            do
            {
                var r = await _table.ExecuteQuerySegmentedAsync(q, continuationToken);

                // queue up this entity in the delete batch if it's one of the rows we need to delete
                foreach (var e in r)
                {
                    if (rowKeysHash.ContainsKey(e.RowKey))
                    {
                        // yep.. this one needs to go
                        if (deleteBatches[currentBatch].Count >= maxBatchSize)
                        {
                            currentBatch++;
                            deleteBatches[currentBatch] = new List<DynamicTableEntity>();
                        };
                        deleteBatches[currentBatch].Add(e);
                    }
                }

                continuationToken = r.ContinuationToken;
            } while (continuationToken != null);

            // delete them in batches of 100
            foreach (var batch in deleteBatches.Keys)
            {
                var batchOperation = new TableBatchOperation();
                var thisbatch = deleteBatches[batch];
                foreach (var e in thisbatch) batchOperation.Delete(e);

                var executeBatchResult = await _table.ExecuteBatchAsync(batchOperation);

                result.AddRange(executeBatchResult);
            }

            return result;
        }

        /// <summary>
        /// deletes one row
        /// </summary>
        public async Task<TableResult> Delete(string partitionKey, string rowKey)
        {
            var entity = await Get<TableEntity>(partitionKey, rowKey);

            if (entity != null)
            {
                var op = TableOperation.Delete(entity);
                return await _table.ExecuteAsync(op);
            }
            return null;
        }

        /// <summary>
        /// deletes a row and returns what was there before
        /// </summary>
        public async Task<T> Pop<T>(string partitionKey, string rowKey) where T : ITableEntity, new()
        {
            var entity = await Get<T>(partitionKey, rowKey);

            var op = TableOperation.Delete(entity);
            var r = await _table.ExecuteAsync(op);

            return entity;
        }

        /// <summary>
        /// </summary>
        public async Task<TableResult> InsertJsonData<T>(string partitionKey, string rowKey, T data)
        {
            return await Insert(new SimpleDataModel()
            {
                PartitionKey = partitionKey,
                RowKey = rowKey,
                Data = JsonConvert.SerializeObject(data)
            });
        }

        public async Task<IList<TableResult>> InsertOrReplaceBatchJsonData<T>(string partitionKey, Func<T, string> rowKeyFun, IReadOnlyList<T> data)
        {
            return await InsertOrReplaceBatch(data.Select(d => new SimpleDataModel()
            {
                PartitionKey = partitionKey,
                RowKey = rowKeyFun(d),
                Data = JsonConvert.SerializeObject(d)
            }).ToList());
        }

        /// <summary>
        /// </summary>
        public async Task<TableResult> InsertOrReplaceJsonData<T>(string partitionKey, string rowKey, T data)
        {
            return await InsertOrReplace(new SimpleDataModel()
            {
                PartitionKey = partitionKey,
                RowKey = rowKey,
                Data = JsonConvert.SerializeObject(data)
            });
        }

        /// <summary>
        /// </summary>
        [Obsolete("Use InsertOrReplaceJsonData instead (same thing, better name)")]
        public async Task UpsertJsonData<T>(string partitionKey, string rowKey, T data)
        {
            await InsertOrReplace(new SimpleDataModel()
            {
                PartitionKey = partitionKey,
                RowKey = rowKey,
                Data = JsonConvert.SerializeObject(data)
            });
        }

        /// <summary>
        /// </summary>
        public async Task<T> GetJsonData<T>(string partitionKey, string rowKey)
        {
            var entity = await Get<SimpleDataModel>(partitionKey, rowKey);

            if (entity == null) return default(T);

            return JsonConvert.DeserializeObject<T>(entity.Data);
        }

        public async Task<IReadOnlyList<T>> GetJsonData<T>(string partitionKey)
        {
            var entitys = await Get<SimpleDataModel>(partitionKey);

            if (entitys == null) return null;

            return entitys.Select(e => JsonConvert.DeserializeObject<T>(e.Data)).ToList();
        }

        /// <summary>
        /// deletes an entire partition
        /// NOTE: this could become very inefficient for a large number of rows
        /// </summary>
        public async Task<IList<TableResult>> Delete(string partitionKey)
        {
            var result = new List<TableResult>();
            var entities = await Get<TableEntity>(partitionKey);
            var batches = entities.SplitToBatches(maxBatchSize).ToList();

            foreach (var batch in batches)
            {
                var batchOperation = new TableBatchOperation();

                foreach (var doc in batch)
                {
                    batchOperation.Delete(doc);
                }
                var rr = await _table.ExecuteBatchAsync(batchOperation);
                result.AddRange(rr);
            }
            return result;
        }

    }

    public class SimpleDataModel : TableEntity
    {
        public string Data { get; set; }
    }

}
