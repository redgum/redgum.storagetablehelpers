﻿using Microsoft.WindowsAzure.Storage.Queue;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Redgum.StorageTableHelpers
{
    public class DeadLettersTable
    {
        private readonly IStorageTableClient _table;
        private readonly TableAccess _tableAccess;

        public DeadLettersTable(TableAccess tableAccess)
        {
            _tableAccess = tableAccess;
            _table = _tableAccess.GetStorageTableClient("deadLetters", ensureExists: true);
        }

        public async Task Store(
            string queueName,
            string messageId,
            string messageText,
            int dequeueCount,
            DateTimeOffset? insertionTime,
            string extraInfo = ""
            )
        {
            await Store(new DeadLetter()
            {
                PartitionKey = queueName,
                RowKey = $"{DateTime.UtcNow.FormatForTableOrdering()} {messageId} {Guid.NewGuid()}",
                MessageText = messageText,
                DequeueCount = dequeueCount,
                InsertionTime = insertionTime,
                ExtraInfo = extraInfo,
            });
        }
        async Task Store(DeadLetter item)
        {
            try
            {
                await _table.InsertOrReplace(item);
            }
            catch (Microsoft.WindowsAzure.Storage.StorageException)
            {
                //according to the documentation string are stored
                //A UTF-16-encoded value. String values may be up to 64 KB in size. 
                //Note that the maximum number of characters supported is about 32 K or less.
                //so Max String length is 32k characters or less
                //lets trim some strings, its better to have some of the string than none of the string - the dead letter table isn't going to be re-parsed by a machine anyway
                const int maxStringLength = 31500;//we're a bit below 32k here, thats on purpose
                if (!(string.IsNullOrEmpty(item.MessageText)) && item.MessageText.Length > maxStringLength)
                {
                    item.MessageText = item.MessageText.Substring(0, maxStringLength);
                }
                if (!(string.IsNullOrEmpty(item.ExtraInfo)) && item.ExtraInfo.Length > maxStringLength)
                {
                    item.ExtraInfo = item.ExtraInfo?.Substring(0, 31500);
                }
                //it'd be nice to write some trace message or something here

                //try to insert again
                //if that fails, we have no choice but to let the exception get thrown
                await _table.InsertOrReplace(item);
            }
        }

        class DeadLetter : TableEntity
        {
            public int DequeueCount { get; set; }
            public DateTimeOffset? InsertionTime { get; set; }
            public string MessageText { get; set; }
            public string ExtraInfo { get; set; }
        }
    }

    public static class DeadLetterTableExtensions
    {
        //public static async Task Store(
        //    this DeadLettersTable table,
        //    string queueName,
        //    dynamic queueEntity,
        //    int dequeueCount,
        //    DateTimeOffset? insertionTime,
        //    string extraInfo = ""
        //    )
        //{
        //    if (queueEntity == null) return;

        //    string json = JsonConvert.SerializeObject(queueEntity);
        //    //log errors
        //    await table.Store(queueName, json, dequeueCount, insertionTime, extraInfo);
        //}

        public static async Task Store(
            this DeadLettersTable table,
            string queueName,
            CloudQueueMessage message,
            string extraInfo = ""
            )
        {
            if (table == null) return;

            await table.Store(
                queueName,
                message.Id,
                message.AsString,
                message.DequeueCount,
                message.InsertionTime,
                extraInfo
                );
        }

        public static async Task Store<TModel>(
            this DeadLettersTable table,
            string queueName,
            OrderedQueueEntity<TModel> item,
            string extraInfo = ""
            ) where TModel : new()
        {
            if (item == null) return;

            string json = JsonConvert.SerializeObject(item);
            //log errors
            await table.Store(
                queueName: queueName,
                messageId: $"{item.PartitionKey}-{item.RowKey}",
                messageText: json,
                dequeueCount: item.DequeueCount,
                insertionTime: item.Timestamp,
                extraInfo: extraInfo
                );
        }
    }
}
