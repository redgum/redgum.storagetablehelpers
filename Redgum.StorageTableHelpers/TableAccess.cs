﻿using Microsoft.Extensions.Options;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Queue;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;

namespace Redgum.StorageTableHelpers
{
    public class AzureSettings
    {
        public string StorageConnection { get; set; }
    }

    public interface ITableAccess
    {
        IStorageTableClient GetStorageTableClient(string tableName, bool ensureExists = false);
        Task<IStorageTableClient> GetStorageTableClientAsync(string tableName, bool ensureExists = false);
        CloudTable GetCloudTable(string tableName, bool ensureExists = false);
        Task<CloudTable> GetCloudTableAsync(string tableName, bool ensureExists = false);
        Task EnsureTableExists(string tableName);
        Task DeleteTable(string tableName);
    }

    public class TableAccess : ITableAccess
    {
        private readonly CloudTableClient _tableClient;

        public TableAccess(IOptions<AzureSettings> config)
        {
            var storageAccount = CloudStorageAccount.Parse(config.Value.StorageConnection);
            _tableClient = storageAccount.CreateCloudTableClient();
        }

        [Obsolete("Use GetStorageTableClient which returns an IStorageTableClient instead of a StorageTableClient")]
        public StorageTableClient GetTableClient(string tableName, bool ensureExists = false)
        {
            var table = GetCloudTable(tableName, ensureExists);
            return new StorageTableClient(table);
        }
        public IStorageTableClient GetStorageTableClient(string tableName, bool ensureExists = false)
        {
            var table = GetCloudTable(tableName, ensureExists);
            return new StorageTableClient(table);
        }

        [Obsolete("Use GetStorageTableClientAsync which returns an IStorageTableClient instead of a StorageTableClient")]
        public async Task<StorageTableClient> GetTableClientAsync(string tableName, bool ensureExists = false)
        {
            var table = await GetCloudTableAsync(tableName, ensureExists);
            return new StorageTableClient(table);
        }
        public async Task<IStorageTableClient> GetStorageTableClientAsync(string tableName, bool ensureExists = false)
        {
            var table = await GetCloudTableAsync(tableName, ensureExists);
            return new StorageTableClient(table);
        }
        public CloudTable GetCloudTable(string tableName, bool ensureExists = false)
        {
            var table = _tableClient.GetTableReference(tableName);
            if (ensureExists) table.CreateIfNotExistsAsync().Wait();
            return table;
        }
        public async Task<CloudTable> GetCloudTableAsync(string tableName, bool ensureExists = false)
        {
            var table = _tableClient.GetTableReference(tableName);
            if (ensureExists) await table.CreateIfNotExistsAsync();
            return table;
        }

        public async Task EnsureTableExists(string tableName)
        {
            var table = _tableClient.GetTableReference(tableName);

            await table.CreateIfNotExistsAsync();
        }

        public async Task DeleteTable(string tableName)
        {
            var table = _tableClient.GetTableReference(tableName);

            await table.DeleteIfExistsAsync();
        }

    }

    public interface IQueueAccess
    {
        CloudQueue GetCloudQueue(string queueName, bool ensureExists = false);
        Task<CloudQueue> GetCloudQueueAsync(string queueName, bool ensureExists = false);
        Task EnsureQueueExists(string queueName);
        Task DeleteQueue(string queueName);
    }
    public class QueueAccess : IQueueAccess
    {
        private readonly CloudQueueClient _queueClient;

        public QueueAccess(IOptions<AzureSettings> config)
        {
            var storageAccount = CloudStorageAccount.Parse(config.Value.StorageConnection);
            _queueClient = storageAccount.CreateCloudQueueClient();
        }

        public CloudQueue GetCloudQueue(string queueName, bool ensureExists = false)
        {
            var queue = _queueClient.GetQueueReference(queueName);
            if (ensureExists) queue.CreateIfNotExistsAsync().Wait();
            return queue;
        }
        public async Task<CloudQueue> GetCloudQueueAsync(string queueName, bool ensureExists = false)
        {
            var queue = _queueClient.GetQueueReference(queueName);
            if (ensureExists) await queue.CreateIfNotExistsAsync();
            return queue;
        }

        public async Task EnsureQueueExists(string queueName)
        {
            var queue = await GetCloudQueueAsync(queueName, ensureExists: true);
        }

        public async Task DeleteQueue(string queueName)
        {
            var queue = await GetCloudQueueAsync(queueName);
            await queue.DeleteIfExistsAsync();
        }
    }

    public class BlobAccess
    {
        private readonly CloudBlobClient _blobClient;
        public BlobAccess(IOptions<AzureSettings> config)
        {
            var storageAccount = CloudStorageAccount.Parse(config.Value.StorageConnection);
            _blobClient = storageAccount.CreateCloudBlobClient();
        }
        public async Task<CloudBlobContainer> GetBlobContainer(string blobContainerName, bool ensureExists = false)
        {
            var blobContainer = _blobClient.GetContainerReference(blobContainerName);
            if (ensureExists) await blobContainer.CreateIfNotExistsAsync();
            return blobContainer;
        }
        public async Task<CloudBlockBlob> GetBlockBlob(string blobContainerName, string blobFileName)
        {
            var blobContainer = await GetBlobContainer(blobContainerName, ensureExists: true);
            return blobContainer.GetBlockBlobReference(blobFileName);
        }

        public async Task<byte[]> DownloadBlobFile(string blobContainerName, string blobFileName)
        {
            var blobContainer = await GetBlobContainer(blobContainerName, ensureExists: true);
            var blobFile = blobContainer.GetBlockBlobReference(blobFileName);

            //populates the blob properties so we can get the length of the blob
            await blobFile.FetchAttributesAsync();
            var bytes = new byte[blobFile.Properties.Length];

            await blobFile.DownloadToByteArrayAsync(bytes, 0);
            return bytes;
        }
    }
}
