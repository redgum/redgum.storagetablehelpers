﻿using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Redgum.StorageTableHelpers
{

    /// <summary>
    /// base class to make it easy to create tables that do basic CRUD 
    /// using a type as the primary key and id as the row key
    /// </summary>
    public class SimpleTypedJsonStorageTable<T>
    {
        private readonly IStorageTableClient _storageClient;
        private readonly string _keyType;
        Func<T, string> _getKeyValueFun;

        public SimpleTypedJsonStorageTable(
            string keyType,
            IStorageTableClient storageClient,
            Func<T, string> getKeyValueFun
            )
        {
            _storageClient = storageClient;
            _keyType = keyType;
            _getKeyValueFun = getKeyValueFun;
        }

        /// <summary>
        /// saves (upserts) the object 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public async Task Store(T obj)
        {
            var keyValue = _getKeyValueFun(obj);

            await _storageClient.UpsertJsonData(_keyType, keyValue, obj);
        }

        public async Task StoreBatch(IReadOnlyList<T> obj)
        {
            await _storageClient.InsertOrReplaceBatchJsonData(_keyType, _getKeyValueFun, obj);
        }

        public async Task<T> Get(string id)
        {
            return await _storageClient.GetJsonData<T>(_keyType, id);
        }

        public async Task Delete(string id)
        {
            await _storageClient.Delete(_keyType, id);
        }

        public async Task DeleteAll()
        {
            await _storageClient.Delete(_keyType);
        }

        public async Task<IReadOnlyList<T>> List()
        {
            return await _storageClient.GetJsonData<T>(_keyType);
        }

        public async Task<bool> Exists(string id)
        {
            return await _storageClient.Exists(_keyType, id);
        }


    }

    /// <summary>
    /// base class to make it easy to create tables that do basic CRUD 
    /// using a type as the primary key and id as the row key
    /// </summary>
    public class SingleRowTypedJsonStorageTable<T>
    {
        private readonly IStorageTableClient _storageClient;
        string _partitionName;
        string _rowKeyName;

        public SingleRowTypedJsonStorageTable(
            string partitionName,
            string rowKeyName,
            IStorageTableClient storageClient
            )
        {
            _storageClient = storageClient;
            _partitionName = partitionName;
            _rowKeyName = rowKeyName;
        }

        /// <summary>
        /// saves (upserts) the object 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public async Task Store(T obj)
        {
            await _storageClient.UpsertJsonData(_partitionName, _rowKeyName, obj);
        }

        public async Task<T> Get()
        {
            return await _storageClient.GetJsonData<T>(_partitionName, _rowKeyName);
        }

        public async Task Delete()
        {
            await _storageClient.Delete(_partitionName, _rowKeyName);
        }

        public async Task<bool> Exists()
        {
            return await _storageClient.Exists(_partitionName, _rowKeyName);
        }


    }

}
