﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Redgum.StorageTableHelpers
{
    /// <summary>
    /// Various utilities and extensions to help with using Azure storage tables
    /// </summary>
    public static class CloudTableUtils
    {

        public static string FormatForTableOrdering(this DateTime d)
        {
            return String.Format("{0:D19}", d.Ticks);
        }


        public static IEnumerable<List<T>> SplitToBatches<T>(this IEnumerable<T> sequence, int size)
        {
            List<T> partition = new List<T>(sequence.Count());
            foreach (var item in sequence)
            {
                partition.Add(item);
                if (partition.Count == size)
                {
                    yield return partition;
                    partition = new List<T>(size);
                }
            }
            if (partition.Count > 0) yield return partition;
        }



        public static TableQuery CreateMinimalQuery(string filter)
        {
            return new TableQuery()
                .Where(filter)
                .Select(new List<string> { "PartitionKey", "RowKey", "Timestamp" })
                ;
        }

        public static async Task<bool> ExecuteExistsQuery(this CloudTable table, string filter)
        {
            var query = CreateMinimalQuery(filter);
            var r = await table.ExecuteQuerySegmentedAsync(query, null);
            return r.Any();
        }

        public static async Task<int> ExecuteCountQuery(this CloudTable table, TableQuery query)
        {
            var count = 0;
            TableContinuationToken continuationToken = null;
            do
            {
                var r = await table.ExecuteQuerySegmentedAsync(query, continuationToken);

                count += r.Results.Count;

                continuationToken = r.ContinuationToken;
            } while (continuationToken != null);

            return count;
        }

        public static string CreateFilter(string partitionKey)
        {
            return TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, partitionKey);
        }

        public static string CreateFilter(string partitionKey, string rowKey)
        {
            return TableQuery.CombineFilters(
                    CreateFilter(partitionKey),
                    TableOperators.And,
                    TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.Equal, rowKey)
                );
        }

        public static string CreateRangeFilter(string partitionKey, string rowKeyFrom, string rowKeyTo, bool inclusive)
        {
            return TableQuery.CombineFilters(
                    CreateFilter(partitionKey),
                    TableOperators.And,
                    GetRowKeyRangeFilter(rowKeyFrom, rowKeyTo, inclusive)
                );
        }

        internal static string GetRowKeyRangeFilter(string rowKeyFrom, string rowKeyTo, bool inclusive)
        {
            return inclusive ? GetInclusiveRowKeyRangeFilter(rowKeyFrom, rowKeyTo) : GetExclusiveRowKeyRangeFilter(rowKeyFrom, rowKeyTo);
        }
        internal static string GetInclusiveRowKeyRangeFilter(string rowKeyFrom, string rowKeyTo)
        {
            return TableQuery.CombineFilters(
                TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.GreaterThanOrEqual, rowKeyFrom),
                TableOperators.And,
                TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.LessThanOrEqual, rowKeyTo)
            );
        }
        internal static string GetExclusiveRowKeyRangeFilter(string rowKeyFrom, string rowKeyTo)
        {
            return TableQuery.CombineFilters(
                TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.GreaterThanOrEqual, rowKeyFrom),
                TableOperators.And,
                TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.LessThan, rowKeyTo)
            );
        }


        //public static string CreateFilter(string partitionKey, IEnumerable<string> rowKeys)
        //{
        //    return TableQuery.CombineFilters(
        //            CreateFilter(partitionKey),
        //            TableOperators.And,
        //            TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.Equal, rowKey)
        //        );
        //}

        /// <summary>
        /// returns all pages of the query
        /// </summary>
        public static async Task<IList<T>> ExecuteQueryAsync<T>(this CloudTable table, TableQuery<T> query, CancellationToken ct = default(CancellationToken), Action<IList<T>> onProgress = null) where T : ITableEntity, new()
        {
            var items = new List<T>();
            TableContinuationToken token = null;

            do
            {
                var seg = await table.ExecuteQuerySegmentedAsync(query, token);
                token = seg.ContinuationToken;
                items.AddRange(seg);
                if (onProgress != null) onProgress(items);

            } while (token != null && !ct.IsCancellationRequested);

            return items;
        }
    }
}
