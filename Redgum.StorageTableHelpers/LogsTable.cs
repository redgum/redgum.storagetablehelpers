﻿using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Redgum.StorageTableHelpers
{
    public class LogsTable
    {
        private readonly IStorageTableClient _table;
        private readonly TableAccess _tableAccess;

        public LogsTable(TableAccess tableAccess)
        {
            _tableAccess = tableAccess;
            _table = _tableAccess.GetStorageTableClient("logs", true);
        }

        public async Task Store(
            string logText,
            string source
            )
        {
            var currentDateTime = DateTime.UtcNow;
            await Store(new Log()
            {
                PartitionKey = currentDateTime.ToString("yyyy-MM-dd"),
                RowKey = $"{currentDateTime.FormatForTableOrdering()} {Guid.NewGuid()}",
                LogText = logText,
                Source = source
            });
        }
        async Task Store(Log item)
        {
            await _table.InsertOrReplace(item);
        }

        class Log : TableEntity
        {
            public string Source { get; set; }
            public string LogText { get; set; }
        }
    }

    public static class LogsTableExtensions
    {
        public static async Task Store(
            this LogsTable table,
            Exception ex,
            string source
            )
        {
            if (table == null) return;
            await table.Store(
                ex.ToString(),
                source
                );
        }

        public static async Task Store(
            this LogsTable table,
            dynamic errors,
            string source
            )
        {
            if (table == null) return;
            if (errors == null) return;

            //log away any errors that might have been returned from SparkPost
            string json = JsonConvert.SerializeObject(errors);

            //log errors
            await table.Store(json, source);
        }
    }
}
