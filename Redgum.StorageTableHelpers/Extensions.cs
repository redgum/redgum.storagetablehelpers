﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Redgum.StorageTableHelpers
{
    public static class Extensions
    {

        public static void AddStorageTableHelpers(this IServiceCollection services)
        {
            services.AddSingleton<TableAccess>();
            services.AddSingleton<ITableAccess, TableAccess>();
            services.AddSingleton<QueueAccess>();
            services.AddSingleton<IQueueAccess, QueueAccess>();
            services.AddSingleton<BlobAccess>();
        }


        /// <summary>
        /// handy util if you need to partition data by the first character
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string GetFirstCharacter(this string s)
        {
            if (string.IsNullOrEmpty(s)) return s;
            return s.First().ToString();
        }


    }
}
