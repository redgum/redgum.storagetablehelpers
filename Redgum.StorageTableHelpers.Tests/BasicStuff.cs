using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.WindowsAzure.Storage.Table;
using Redgum.StorageTableHelpers.Tests.Helpers;
using System;
using System.IO;
using System.Threading.Tasks;

namespace Redgum.StorageTableHelpers.Tests
{
    [TestClass]
    public class UnitTest1
    {



        [TestMethod]
        public async Task TableCreatedAndAddDataImmediatly()
        {

            var tableAccess = Utils.GetTableAccess();

            var r = new Random((int)(DateTime.Now.Ticks % 99999));
            // note.. if we use the same table name each time..
            // we might run into issues creating the table while the last one was deleting
            // this can take up to 40 seconds
            // https://docs.microsoft.com/en-us/rest/api/storageservices/Delete-Table?redirectedfrom=MSDN
            var name = "test1234create" + r.Next(999999);

            try
            {
                var t = await tableAccess.GetStorageTableClientAsync(name);
                // this should wait until the table is created
                await tableAccess.EnsureTableExists(name);

                await t.Insert(new TableEntity()
                {
                    PartitionKey = "dfgdf",
                    RowKey = "dfgsdf"
                });

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            finally
            {
                await tableAccess.DeleteTable(name);
            }
        }

        [TestMethod]
        public async Task SaveAndLoadWithRange()
        {
            var tableAccess = Utils.GetTableAccess();

            var r = new Random((int)(DateTime.Now.Ticks % 99999));
            // note.. if we use the same table name each time..
            // we might run into issues creating the table while the last one was deleting
            // this can take up to 40 seconds
            // https://docs.microsoft.com/en-us/rest/api/storageservices/Delete-Table?redirectedfrom=MSDN
            var name = "test1234create" + r.Next(999999);
            var partitionKey = "ABC123";

            try
            {
                var t = await tableAccess.GetStorageTableClientAsync(name);
                await tableAccess.EnsureTableExists(name);


                var date1 = new DateTime(2001, 1, 1);
                var date2 = new DateTime(2002, 1, 1);
                var date3 = new DateTime(2003, 1, 1);

                var doc1 = new Tester()
                {
                    PartitionKey = partitionKey,
                    RowKey = date1.FormatForTableOrdering(),
                    Hello = 1,
                };

                var doc2 = new Tester()
                {
                    PartitionKey = partitionKey,
                    RowKey = date2.FormatForTableOrdering(),
                    Hello = 2,
                };

                var doc3 = new Tester()
                {
                    PartitionKey = partitionKey,
                    RowKey = date3.FormatForTableOrdering(),
                    Hello = 3,
                };

                await t.Insert(doc1);
                await t.Insert(doc2);
                await t.Insert(doc3);

                // big date range should return all 3
                var all = await t.Get<Tester>(partitionKey,
                    new DateTime(1900, 1, 1).FormatForTableOrdering(),
                    new DateTime(3000, 1, 1).FormatForTableOrdering(),
                    true);

                Assert.IsTrue(all.Count == 3);

                // lets try getting a subset
                // skip the last one from 2003
                var some = await t.Get<Tester>(partitionKey,
                    new DateTime(1900, 1, 1).FormatForTableOrdering(),
                    new DateTime(2002, 1, 1).FormatForTableOrdering(),
                    true);

                Assert.IsTrue(some.Count == 2);

                // check the count call while we're at it
                // use an inclusive search
                var countSome = await t.GetCount(partitionKey,
                    new DateTime(1900, 1, 1).FormatForTableOrdering(),
                    new DateTime(2002, 1, 1).FormatForTableOrdering(),
                    true);

                Assert.IsTrue(countSome == 2);

                // does inclusive work?
                // turn it off and find out
                var countSomeExc = await t.GetCount(partitionKey,
                    new DateTime(1900, 1, 1).FormatForTableOrdering(),
                    new DateTime(2002, 1, 1).FormatForTableOrdering(),
                    false);

                Assert.IsTrue(countSomeExc == 1);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            finally
            {
                await tableAccess.DeleteTable(name);
            }
        }

        [TestMethod]
        public async Task GetAllFromAllPartitions()
        {
            var tableAccess = Utils.GetTableAccess();

            var r = new Random((int)(DateTime.Now.Ticks % 99999));
            // note.. if we use the same table name each time..
            // we might run into issues creating the table while the last one was deleting
            // this can take up to 40 seconds
            // https://docs.microsoft.com/en-us/rest/api/storageservices/Delete-Table?redirectedfrom=MSDN
            var name = "test1234create" + r.Next(999999);
            var partitionKey = "ABC123";

            try
            {
                var t = await tableAccess.GetStorageTableClientAsync(name);
                await tableAccess.EnsureTableExists(name);

                var doc1 = new Tester()
                {
                    PartitionKey = "1",
                    RowKey = "dfgsdfg",
                    Hello = 1,
                };

                var doc2 = new Tester()
                {
                    PartitionKey = "2",
                    RowKey = "dfgsdfgdfgsdfg",
                    Hello = 2,
                };

                var doc3 = new Tester()
                {
                    PartitionKey = partitionKey,
                    RowKey = "fghghjgfkhj",
                    Hello = 3,
                };

                await t.Insert(doc1);
                await t.Insert(doc2);
                await t.Insert(doc3);

                var all = await t.GetAll<Tester>();
                Assert.IsTrue(all.Count == 3);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            finally
            {
                await tableAccess.DeleteTable(name);
            }
        }


        class Tester : TableEntity
        {
            public int Hello { get; set; }
        }
    }
}
