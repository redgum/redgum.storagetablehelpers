﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Redgum.StorageTableHelpers.Tests.Helpers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Redgum.StorageTableHelpers.Tests
{
    [TestClass]
    public class SingleTypedStorageTableTests
    {


        [TestMethod]
        public async Task SingleJSONTableEverything()
        {
            // test the json table thingy with a real db.. test all functions in one go

            var tableAccess = Utils.GetTableAccess();

            var r = new Random((int)(DateTime.Now.Ticks % 99999));
            var name = "test1234jsontester" + r.Next(999999);

            var table = new SingleRowTypedJsonStorageTable<TestClass>("SOMEKEY","SOMEOTHERKEY", tableAccess.GetStorageTableClient(name, true));

            var data1 = new TestClass()
            {
                IntOne = 1,
                IntTwo = 2,
                StringOne = "dfgsdfg",
                StringTwo = "zzzzz",
            };


            var data2 = new TestClass()
            {
                IntOne = 6,
                IntTwo = 6,
                StringOne = "777",
                StringTwo = "888",
            };

            await table.Store(data1);
            await table.Store(data2);

            // does it load back ok?
            var back = await table.Get();
            Assert.IsTrue(back.IntOne == data2.IntOne);

            // does it exists?
            var exists = await table.Exists();
            Assert.IsTrue(exists);

            // we can delete?
            await table.Delete();
            var deleted = await table.Get();
            Assert.IsTrue(deleted==null);

            // is it gone?
            var stillThere= await table.Exists();
            Assert.IsTrue(!stillThere);

            // clean up
            await tableAccess.DeleteTable(name);
        }


        public static TestClass GetTestClass()
        {
            return new TestClass()
            {
                IntOne = 1,
                IntTwo = 2,
                StringOne = "One",
                StringTwo = "Two"
            };
        }

        public class TestClass
        {
            public int IntOne { get; set; }
            public int IntTwo { get; set; }
            public string StringOne { get; set; }
            public string StringTwo { get; set; }
        }
    }
}
