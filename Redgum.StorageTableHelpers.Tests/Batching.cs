using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using Redgum.StorageTableHelpers.Tests.Helpers;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Redgum.StorageTableHelpers.Tests
{
    [TestClass]
    public class Batching
    {
        [TestMethod]
        public async Task InsertAndDeleteBatch()
        {

            var tableAccess = Utils.GetTableAccess();

            var r = new Random((int)(DateTime.Now.Ticks % 99999));
            // note.. if we use the same table name each time..
            // we might run into issues creating the table while the last one was deleting
            // this can take up to 40 seconds
            // https://docs.microsoft.com/en-us/rest/api/storageservices/Delete-Table?redirectedfrom=MSDN
            var name = "test1234create" + r.Next(999999);
            var partitionKey = "ABC123";

            try
            {
                var t = await tableAccess.GetStorageTableClientAsync(name);
                await tableAccess.EnsureTableExists(name);

                var es = Enumerable.Range(0, 1000).Select(ee => new Tester()
                {
                    PartitionKey = partitionKey,
                    RowKey = ee.ToString(),
                    Hello = ee,
                }).ToList();

                await t.InsertBatch(es);

                // get one back
                var e = await t.Get<Tester>(partitionKey, "100");
                Assert.IsTrue(e.RowKey == "100");

                // now add one with this key but not in this batch... this one shouldn't get deleted
                await t.Insert(new Tester()
                {
                    PartitionKey = partitionKey,
                    RowKey = "NODELETE",
                    Hello = 666,
                });

                // delete all the batches
                var keys = es.Select(ee => ee.RowKey).ToList();
                await t.DeleteBatch(partitionKey, keys);

                var all = await t.Get<Tester>(partitionKey);
                Assert.IsTrue(all.Count == 1); // should be one left
                Assert.IsTrue(all.Single().Hello == 666);

            }
            catch (StorageException ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
                throw;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
                throw;
            }
            finally
            {
                await tableAccess.DeleteTable(name);
            }
        }

        class Tester : TableEntity
        {
            public int Hello { get; set; }
        }
    }
}
