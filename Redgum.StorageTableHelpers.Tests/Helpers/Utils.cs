﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Redgum.StorageTableHelpers.Tests.Helpers
{
  public static  class Utils
    {
        public static string GetConnectionString()
        {
            return File.ReadAllText(@"C:\Source\testTableConnectionString.txt").Trim();
        }

        public static TableAccess GetTableAccess()
        {
            var con = GetConnectionString();

            // ensure the table is created before we add data
            var azureSettings = new AzureSettings() { StorageConnection = con };
            var azureOptions = Options.Create(azureSettings);

            return new TableAccess(azureOptions);
        }
    }
}
