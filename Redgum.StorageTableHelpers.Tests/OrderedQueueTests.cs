﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Redgum.StorageTableHelpers.Tests.Helpers;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using Redgum.StorageTableHelpers;
using System.Net.Http;
using Newtonsoft.Json;
using Microsoft.WindowsAzure.Storage.Shared.Protocol;
using System.Net;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Net.Http.Headers;
using static Microsoft.WindowsAzure.Storage.Shared.Protocol.Constants;
using System.Security.Cryptography;
using Microsoft.WindowsAzure.Storage.Auth;

namespace Redgum.StorageTableHelpers.Tests
{

    public class SomeTestDataToSerialize
    {
        public string TheData { get; set; }
    }

    [TestClass]
    public class OrderedQueueTests
    {

        private CloudStorageAccount GetStorageAccount()
        {
            return CloudStorageAccount.Parse(Utils.GetConnectionString());
        }
        private async Task<CloudTable> GetTestTable()
        {
            var storageAccount = GetStorageAccount();
            return await GetTestTable(storageAccount);
        }
        private async Task<CloudTable> GetTestTable(CloudStorageAccount storageAccount)
        {
            var tableName = "testOrderedQueueTable";
            var tableClient = storageAccount.CreateCloudTableClient();
            var table = tableClient.GetTableReference(tableName);
            var createResult = await table.CreateIfNotExistsAsync();
            return table;
        }
        private async Task<CloudTable> GetEmptyTestTable<T>() where T : new()
        {
            var storageAccount = GetStorageAccount();
            return await GetEmptyTestTable<T>(storageAccount);
        }
        private async Task<CloudTable> GetEmptyTestTable<T>(CloudStorageAccount storageAccount) where T : new()
        {
            var table = await GetTestTable(storageAccount);

            //get ALL the entities in the table (so we can delete them)
            var query = new TableQuery<OrderedQueueEntity<T>>();
            var items = await table.ExecuteQueryAsync(query);

            ////delete as a batch
            //var deleteBatchOperation = new TableBatchOperation();
            //foreach (var i in items)
            //    deleteBatchOperation.Delete(i);

            //await table.ExecuteBatchAsync(deleteBatchOperation);
            //A word of caution: a Batch Operation allows a maximum 100 entities in the batch which must share the same PartitionKey so you may need to split the entities into proper batches for this to work.

            foreach (var i in items)
            {
                var operation = TableOperation.Delete(i);
                var deleteResult = await table.ExecuteAsync(operation);
            }
            return table;
        }
        private async Task<OrderedQueue<T>> GetTestQueue<T>() where T : new()
        {
            var table = await GetTestTable();
            return GetTestQueue<T>(table);
        }
        private OrderedQueue<T> GetTestQueue<T>(CloudTable table) where T : new()
        {
            var leaseDuration = TimeSpan.FromSeconds(30);
            return GetTestQueue<T>(table, leaseDuration);
        }
        private OrderedQueue<T> GetTestQueue<T>(CloudTable table, TimeSpan leaseDuration) where T : new()
        {
            var orderedQueue = new OrderedQueue<T>(table, "TestQueue", leaseDuration);
            return orderedQueue;
        }
        private async Task<OrderedQueue<T>> GetEmptyTestQueue<T>() where T : new()
        {
            var table = await GetEmptyTestTable<T>();
            return GetTestQueue<T>(table);
        }

        [TestMethod]
        public async Task AddItemTest()
        {
            var orderedQueue = await GetTestQueue<SomeTestDataToSerialize>();

            var stringData = $"Serialised Data {DateTime.UtcNow}";
            await orderedQueue.AddItem(stringData, visibleAt: DateTime.UtcNow);
        }

        [TestMethod]
        public async Task PeekItemTest()
        {
            var orderedQueue = await GetEmptyTestQueue<SomeTestDataToSerialize>();
            //make sure there's at least 1 item in the queue
            var stringData = $"Serialised Data {DateTime.UtcNow}";
            await orderedQueue.AddItem(stringData, visibleAt: DateTime.UtcNow);

            var peekedItem = await orderedQueue.PeekNextItem();
            Assert.IsNotNull(peekedItem);
            Assert.AreEqual(stringData, peekedItem.Data.TheData);
        }

        [TestMethod]
        public async Task PeekItemPerformanceTest()
        {
            var table = await GetEmptyTestTable<SomeTestDataToSerialize>();
            var orderedQueue = GetTestQueue<SomeTestDataToSerialize>(table);

            //put 100 items in the queue with their visibilityAt set to 1 sec from now, followed by 2 seconds from now, followed by 3 seconds from now, etc
            int itemCount = 100;
            await AddItems(orderedQueue, itemCount, (i) => TimeSpan.FromSeconds(i));

            var watch = System.Diagnostics.Stopwatch.StartNew();
            var peekedItem = await orderedQueue.PeekNextItem();
            watch.Stop();
            Assert.IsNotNull(peekedItem);
            //the PeekNextItem call is subject to network latency, so we want to be a bit lenient on expected speed timings.
            Assert.IsTrue((watch.ElapsedMilliseconds < TimeSpan.FromSeconds(0.2).TotalMilliseconds), $"Peek Item took {watch.ElapsedMilliseconds}, which is to slow with {itemCount} items in the queue");
        }
        [TestMethod]
        public async Task PeekItemMultiInstancePerformanceTest()
        {
            var table1 = await GetEmptyTestTable<SomeTestDataToSerialize>();
            var orderedQueue1 = GetTestQueue<SomeTestDataToSerialize>(table1);
            //make sure we have a completely new instance to work with, in case there's some helpful caching underneath us somewhere
            var table2 = await GetEmptyTestTable<SomeTestDataToSerialize>();
            var orderedQueue2 = GetTestQueue<SomeTestDataToSerialize>(table2);

            //put 100 items in the queue with their visibilityAt set to 1 sec from now, followed by 2 seconds from now, followed by 3 seconds from now, etc
            int itemCount = 100;
            await AddItems(orderedQueue1, itemCount, visibleAtOffsetFromNowFunc: (i) => TimeSpan.FromSeconds(i));


            //work with the second instance, in case there's some 'helpful' caching underneath us somewhere
            var watch = System.Diagnostics.Stopwatch.StartNew();
            var peekedItem = await orderedQueue2.PeekNextItem();
            watch.Stop();
            Assert.IsNotNull(peekedItem);
            //the PeekNextItem call is subject to network latency, so we want to be a bit lenient on expected speed timings.
            Assert.IsTrue((watch.ElapsedMilliseconds < TimeSpan.FromSeconds(0.2).TotalMilliseconds), $"Peek Item took {watch.ElapsedMilliseconds}, which is to slow with {itemCount} items in the queue");
        }


        [TestMethod]
        public async Task GetNextItemTest()
        {
            var orderedQueue = await GetEmptyTestQueue<SomeTestDataToSerialize>();
            //make sure there's at least 1 item in the queue
            var stringData = $"Serialised Data {DateTime.UtcNow}";
            await orderedQueue.AddItem(stringData, visibleAt: DateTime.UtcNow);

            var item = await orderedQueue.GetNextItem();
            Assert.IsNotNull(item);
            Assert.AreEqual(stringData, item.Data.TheData);
        }


        [TestMethod]
        public async Task GetNextItemEmptyQueueTest()
        {
            var orderedQueue = await GetEmptyTestQueue<SomeTestDataToSerialize>();
            //make sure there's at least 1 item in the queue
            var stringData = $"Serialised Data {DateTime.UtcNow}";
            await orderedQueue.AddItem(stringData, visibleAt: DateTime.UtcNow);

            var item = await orderedQueue.GetNextItem();
            Assert.IsNotNull(item);
            Assert.AreEqual(stringData, item.Data.TheData);
            var item2 = await orderedQueue.GetNextItem();
            Assert.IsNull(item2);
        }


        [TestMethod]
        public async Task GetNextItemManualLeaseTest()
        {
            var table = await GetEmptyTestTable<SomeTestDataToSerialize>();
            var orderedQueue = GetTestQueue<SomeTestDataToSerialize>(table);
            //make sure there's at least 1 item in the queue
            var stringData = $"Serialised Data {DateTime.UtcNow}";
            await orderedQueue.AddItem(stringData, visibleAt: DateTime.UtcNow);

            var item = await orderedQueue.PeekNextItem();
            Assert.IsNotNull(item);
            Assert.AreEqual(stringData, item.Data.TheData);

            //this isn't something that would ever be done from a client, its just a test
            item.LeaseIdentifier = Guid.NewGuid();
            item.LeaseExpiresAtUtc = DateTime.UtcNow.Add(TimeSpan.FromSeconds(30));
            var operation = TableOperation.Merge(item);
            //attempt to acquire the lease 
            var updateResult = await table.ExecuteAsync(operation);

            //should get nothing
            var item2 = await orderedQueue.GetNextItem();
            Assert.IsNull(item2);
        }

        [TestMethod]
        public async Task GetNextItemMultiTimesWithObtainLeaseTest()
        {
            var table = await GetEmptyTestTable<SomeTestDataToSerialize>();
            var orderedQueue = GetTestQueue<SomeTestDataToSerialize>(table);

            //make sure we've got some items in the queue
            int itemCount = 3;
            await AddItems(orderedQueue, itemCount);


            var item = await orderedQueue.PeekNextItem();
            Assert.IsNotNull(item);

            var result1 = await orderedQueue.ObtainLease(item);
            Assert.IsNotNull(result1);
            var result2 = await orderedQueue.ObtainLease(item);
            Assert.IsNull(result2);
        }

        [TestMethod]
        public async Task GetNextItemMultiInstanceTest()
        {
            var table1 = await GetEmptyTestTable<SomeTestDataToSerialize>();
            var orderedQueue1 = GetTestQueue<SomeTestDataToSerialize>(table1);
            var table2 = await GetEmptyTestTable<SomeTestDataToSerialize>();
            var orderedQueue2 = GetTestQueue<SomeTestDataToSerialize>(table2);
            //make sure there's at least 1 item in the queue
            var stringData = $"Serialised Data {DateTime.UtcNow}";
            await orderedQueue1.AddItem(stringData, visibleAt: DateTime.UtcNow);

            var item1 = await orderedQueue1.PeekNextItem();
            Assert.IsNotNull(item1);
            System.Diagnostics.Debug.WriteLine("Item etag {0}", item1.ETag);

            var item2 = await orderedQueue2.PeekNextItem();
            Assert.IsNotNull(item2);
            System.Diagnostics.Debug.WriteLine("Item etag {0}", item2.ETag);

            //the items should be different instances, but they will have the same value for an ETag
            Assert.AreNotSame(item1, item2);
            Assert.AreEqual(item1.ETag, item2.ETag);


            //this isn't something that would ever be done from a client
            //ObtainLease is not available for general consumption
            var leasedItem1 = await orderedQueue1.ObtainLease(item1);
            Assert.IsNotNull(leasedItem1);

            //we shouldn't be able to get a lease on the same item a second time
            var leasedItem2 = await orderedQueue2.ObtainLease(item2);
            Assert.IsNull(leasedItem2);

            //should get nothing
            var nextItem1 = await orderedQueue1.GetNextItem();
            Assert.IsNull(nextItem1);
            //should get nothing
            var nextItem2 = await orderedQueue2.GetNextItem();
            Assert.IsNull(nextItem2);
        }

        [TestMethod]
        public async Task GetNextItemMultiInstanceWithItemModificationTest()
        {
            var table1 = await GetEmptyTestTable<SomeTestDataToSerialize>();
            var orderedQueue1 = GetTestQueue<SomeTestDataToSerialize>(table1);
            var table2 = await GetEmptyTestTable<SomeTestDataToSerialize>();
            var orderedQueue2 = GetTestQueue<SomeTestDataToSerialize>(table2);
            //make sure there's at least 1 item in the queue
            await orderedQueue1.AddItem("Serialised Data", visibleAt: DateTime.UtcNow);

            var item1 = await orderedQueue1.PeekNextItem();
            Assert.IsNotNull(item1);
            System.Diagnostics.Debug.WriteLine("Item etag {0}", item1.ETag);

            var item2 = await orderedQueue2.PeekNextItem();
            Assert.IsNotNull(item2);
            System.Diagnostics.Debug.WriteLine("Item etag {0}", item2.ETag);

            //the items should be different instances, but they will have the same value for an ETag
            Assert.AreNotSame(item1, item2);
            Assert.AreEqual(item1.ETag, item2.ETag);

            //altering item2 should mean that we can't get a lease on item 1 because it's eTag will be outdated
            await ModifyItemAndMerge(table2, item2);

            //this isn't something that would ever be done from a client
            //ObtainLease is not available for general consumption
            var leasedItem1 = await orderedQueue1.ObtainLease(item1);
            Assert.IsNull(leasedItem1);

            //but we should still be able to get the lease on item 2, because it'll be in sync with the DB and have an updated eTag
            var leasedItem2 = await orderedQueue2.ObtainLease(item2);
            Assert.IsNotNull(leasedItem2);

            //should get nothing
            var nextItem1 = await orderedQueue1.GetNextItem();
            Assert.IsNull(nextItem1);
            //should get nothing
            var nextItem2 = await orderedQueue2.GetNextItem();
            Assert.IsNull(nextItem2);
        }

        [TestMethod]
        public async Task GetNextItemMultiInstanceConcurrencyTest()
        {
            var table1 = await GetEmptyTestTable<SomeTestDataToSerialize>();
            var orderedQueue1 = GetTestQueue<SomeTestDataToSerialize>(table1);
            var table2 = await GetEmptyTestTable<SomeTestDataToSerialize>();
            var orderedQueue2 = GetTestQueue<SomeTestDataToSerialize>(table2);

            //make sure we've got some items in the queue
            int itemCount = 10;
            await AddItems(orderedQueue1, itemCount);

            for (int i = 0; i < (itemCount / 2); i++)
            {
                var t1 = orderedQueue1.GetNextItem();
                var t2 = orderedQueue2.GetNextItem();

                Task.WaitAll(t1, t2);

                var item1 = t1.Result;
                Assert.IsNotNull(item1);

                var item2 = t2.Result;
                Assert.IsNotNull(item2);
                System.Diagnostics.Debug.WriteLine("Item etag {0}", item2.ETag);

                //the items should be completely different
                Assert.AreNotSame(item1, item2);
                Assert.AreNotEqual(item1.ETag, item2.ETag);
            }

            //queue should be empty
            var gotItem1 = await orderedQueue1.GetNextItem();
            Assert.IsNull(gotItem1);
            //queue should be empty
            var gotItem2 = await orderedQueue2.GetNextItem();
            Assert.IsNull(gotItem2);


            //var item1 = await orderedQueue1.PeekNextItem();
            //Assert.IsNotNull(item1);
            //System.Diagnostics.Debug.WriteLine("Item etag {0}", item1.ETag);

            //var item2 = await orderedQueue2.PeekNextItem();
            //Assert.IsNotNull(item2);
            //System.Diagnostics.Debug.WriteLine("Item etag {0}", item2.ETag);

            ////the items should be different instances, but they will have the same value for an ETag
            //Assert.AreNotSame(item1, item2);
            //Assert.AreEqual(item1.ETag, item2.ETag);

            ////altering item2 should mean that we can't get a lease on item 1 because it's eTag will be outdated
            //await ModifyItemAndMerge(table2, item2);

            ////this isn't something that would ever be done from a client
            ////ObtainLease is not available for general consumption
            //var leasedItem1 = await orderedQueue1.ObtainLease(item1);
            //Assert.IsNull(leasedItem1);

            ////but we should still be able to get the lease on item 2, because it'll be in sync with the DB and have an updated eTag
            //var leasedItem2 = await orderedQueue2.ObtainLease(item2);
            //Assert.IsNotNull(leasedItem2);

            ////should get nothing
            //var nextItem1 = await orderedQueue1.GetNextItem();
            //Assert.IsNull(nextItem1);
            ////should get nothing
            //var nextItem2 = await orderedQueue2.GetNextItem();
            //Assert.IsNull(nextItem2);
        }

        [TestMethod]
        public async Task GetItemMultiInstanceAttemptModificationTest()
        {
            var table1 = await GetEmptyTestTable<SomeTestDataToSerialize>();
            var orderedQueue1 = GetTestQueue<SomeTestDataToSerialize>(table1);
            var table2 = await GetEmptyTestTable<SomeTestDataToSerialize>();
            var orderedQueue2 = GetTestQueue<SomeTestDataToSerialize>(table2);

            //make sure we've got some items in the queue
            int itemCount = 3;
            await AddItems(orderedQueue1, itemCount);

            var peekedItem1 = await orderedQueue1.PeekNextItem();
            Assert.IsNotNull(peekedItem1);

            var gotItem2 = await orderedQueue2.GetNextItem();
            Assert.IsNotNull(gotItem2);

            //the items should be different instances, but they will have the same value Partition and Row Keys
            Assert.AreNotSame(peekedItem1, gotItem2);
            Assert.AreEqual(peekedItem1.PartitionKey, gotItem2.PartitionKey);
            Assert.AreEqual(peekedItem1.RowKey, gotItem2.RowKey);

            //modify peeked item 1, should fail because the etag will be out of date for the peekedItem1
            try
            {
                await ModifyItemAndMerge(table2, peekedItem1);
                Assert.Fail("modify peeked item 1, should fail because the etag should be out of date for the peekedItem1");
            }
            catch
            {
                //
            }
        }
        [TestMethod]
        public async Task DeleteItemTest()
        {
            var table = await GetEmptyTestTable<SomeTestDataToSerialize>();
            var orderedQueue = GetTestQueue<SomeTestDataToSerialize>(table, leaseDuration: TimeSpan.FromSeconds(1));

            //make sure we've got some items in the queue
            int itemCount = 1;
            await AddItems(orderedQueue, itemCount);

            var gotItem = await orderedQueue.GetNextItem();
            Assert.IsNotNull(gotItem);
            await orderedQueue.DeleteItem(gotItem);

            //queue should be empty
            var gotItem1 = await orderedQueue.GetNextItem();
            Assert.IsNull(gotItem1);
        }

        [TestMethod]
        public async Task GetItemDeleteItemMultiTest()
        {
            var table = await GetEmptyTestTable<SomeTestDataToSerialize>();
            var orderedQueue = GetTestQueue<SomeTestDataToSerialize>(table, leaseDuration: TimeSpan.FromSeconds(1));

            //make sure we've got some items in the queue
            int itemCount = 3;
            await AddItems(orderedQueue, itemCount);

            var gotItem1 = await orderedQueue.GetNextItem();
            Assert.IsNotNull(gotItem1);
            await orderedQueue.DeleteItem(gotItem1);
            var gotItem2 = await orderedQueue.GetNextItem();
            Assert.IsNotNull(gotItem2);
            await orderedQueue.DeleteItem(gotItem2);
            var gotItem3 = await orderedQueue.GetNextItem();
            Assert.IsNotNull(gotItem3);
            await orderedQueue.DeleteItem(gotItem3);

            //queue should be empty
            var gotItem4 = await orderedQueue.GetNextItem();
            Assert.IsNull(gotItem4);
        }
        [TestMethod]
        public async Task GetItemMultiDeleteItemMultiTest()
        {
            var table = await GetEmptyTestTable<SomeTestDataToSerialize>();
            var orderedQueue = GetTestQueue<SomeTestDataToSerialize>(table, leaseDuration: TimeSpan.FromSeconds(1));

            //make sure we've got some items in the queue
            int itemCount = 3;
            await AddItems(orderedQueue, itemCount);

            var gotItem1 = await orderedQueue.GetNextItem();
            Assert.IsNotNull(gotItem1);
            var gotItem2 = await orderedQueue.GetNextItem();
            Assert.IsNotNull(gotItem2);
            var gotItem3 = await orderedQueue.GetNextItem();
            Assert.IsNotNull(gotItem3);

            await orderedQueue.DeleteItem(gotItem1);
            await orderedQueue.DeleteItem(gotItem2);
            await orderedQueue.DeleteItem(gotItem3);

            //queue should be empty
            var gotItem4 = await orderedQueue.GetNextItem();
            Assert.IsNull(gotItem4);
        }

        [TestMethod]
        public async Task DeleteItemWithTimedOutLeaseTest()
        {
            var table = await GetEmptyTestTable<SomeTestDataToSerialize>();
            var orderedQueue = GetTestQueue<SomeTestDataToSerialize>(table, leaseDuration: TimeSpan.FromSeconds(1));

            //make sure we've got some items in the queue
            int itemCount = 3;
            await AddItems(orderedQueue, itemCount);

            var gotItem = await orderedQueue.GetNextItem();
            Assert.IsNotNull(gotItem);

            await Task.Delay(TimeSpan.FromSeconds(1.1)); //sleep for a bit over 1 second, because that's our Lease Duration
            await orderedQueue.TimeOutExpiredLeases(batchSize: 5);

            //modify peeked item 1, should fail because the etag will be out of date for the peekedItem1
            try
            {
                await orderedQueue.DeleteItem(gotItem);
                Assert.Fail("modify peeked item 1, should fail because the etag should be out of date for the peekedItem1");
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.ToString());
            }
        }


        [TestMethod]
        public async Task TimeOutLeaseWithSecondaryPeekNextItemTest()
        {
            var table = await GetEmptyTestTable<SomeTestDataToSerialize>();
            var orderedQueue = GetTestQueue<SomeTestDataToSerialize>(table, leaseDuration: TimeSpan.FromSeconds(1));

            //make sure we've got some items in the queue
            int itemCount = 3;
            await AddItems(orderedQueue, itemCount);

            var gotItem = await orderedQueue.GetNextItem();
            Assert.IsNotNull(gotItem);
            Assert.AreEqual(1, gotItem.DequeueCount);
            var gotItemPartitionKey = gotItem.PartitionKey;
            var gotItemRowKey = gotItem.RowKey;

            await Task.Delay(TimeSpan.FromSeconds(1.1)); //sleep for a bit over 1 second, because that's our Lease Duration
            await orderedQueue.TimeOutExpiredLeases(batchSize: 5);

            //The Secondary PeekNextItem
            //should get the same item back again
            gotItem = await orderedQueue.PeekNextItem();
            Assert.IsNotNull(gotItem);
            Assert.AreEqual(gotItemPartitionKey, gotItem.PartitionKey);
            Assert.AreEqual(gotItemRowKey, gotItem.RowKey);
            //and its Dequeue Count should have been incremented only once
            Assert.AreEqual(1, gotItem.DequeueCount);
        }

        [TestMethod]
        public async Task TimeOutLeaseWithSecondaryGetItemTest()
        {
            var table = await GetEmptyTestTable<SomeTestDataToSerialize>();
            var orderedQueue = GetTestQueue<SomeTestDataToSerialize>(table, leaseDuration: TimeSpan.FromSeconds(1));

            //make sure we've got some items in the queue
            int itemCount = 3;
            await AddItems(orderedQueue, itemCount);

            var gotItem = await orderedQueue.GetNextItem();
            Assert.IsNotNull(gotItem);
            Assert.AreEqual(1, gotItem.DequeueCount);
            var gotItemPartitionKey = gotItem.PartitionKey;
            var gotItemRowKey = gotItem.RowKey;

            await Task.Delay(TimeSpan.FromSeconds(1.1)); //sleep for a bit over 1 second, because that's our Lease Duration
            await orderedQueue.TimeOutExpiredLeases(batchSize: 5);

            //The Secondary GetItem
            //should get the same item back again
            gotItem = await orderedQueue.GetNextItem();
            Assert.IsNotNull(gotItem);
            Assert.AreEqual(gotItemPartitionKey, gotItem.PartitionKey);
            Assert.AreEqual(gotItemRowKey, gotItem.RowKey);
            //and its Dequeue Count should have been incremented twice
            Assert.AreEqual(2, gotItem.DequeueCount);
        }


        [TestMethod]
        public async Task ApproximateVisibleQueueLengthTest()
        {
            var table = await GetEmptyTestTable<SomeTestDataToSerialize>();
            var orderedQueue = GetTestQueue<SomeTestDataToSerialize>(table);

            //make sure we've got some items in the queue
            int itemCount = 10;
            await AddItems(orderedQueue, itemCount);

            var visibleQueueLength = await orderedQueue.ApproximateVisibleQueueLength();
            Assert.AreEqual(itemCount, visibleQueueLength);
        }

        [TestMethod]
        public async Task ApproximateVisibleQueueLength2Test()
        {
            var table = await GetEmptyTestTable<SomeTestDataToSerialize>();
            var orderedQueue = GetTestQueue<SomeTestDataToSerialize>(table);

            //make sure we've got some items in the queue
            int itemCount = 5;
            await AddItems(orderedQueue, itemCount);
            await AddItems(orderedQueue, itemCount, visibleAtOffsetFromNow: TimeSpan.FromHours(1));

            var visibleQueueLength = await orderedQueue.ApproximateVisibleQueueLength();
            Assert.AreEqual(itemCount, visibleQueueLength);
        }
        [TestMethod]
        public async Task ApproximateVisibleQueueLengthPerformanceTest()
        {
            var table = await GetEmptyTestTable<SomeTestDataToSerialize>();
            var orderedQueue = GetTestQueue<SomeTestDataToSerialize>(table);

            //make sure we've got some items in the queue
            int itemCount = 200;
            await AddItems(orderedQueue, itemCount);

            var watch = System.Diagnostics.Stopwatch.StartNew();
            var visibleQueueLength = await orderedQueue.ApproximateVisibleQueueLength();
            watch.Stop();
            Assert.AreEqual(itemCount, visibleQueueLength);
            //all calls are subject to network latency, so we want to be a bit lenient on expected speed timings.
            Assert.IsTrue((watch.ElapsedMilliseconds < TimeSpan.FromSeconds(0.2).TotalMilliseconds), $"ApproximateVisibleQueueLength() took {watch.ElapsedMilliseconds}, which is to slow with {itemCount} items in the queue");
        }

        [TestMethod]
        public async Task ApproximateVisibleQueueLength2PerformanceTest()
        {
            var table = await GetEmptyTestTable<SomeTestDataToSerialize>();
            var orderedQueue = GetTestQueue<SomeTestDataToSerialize>(table);

            //make sure we've got some items in the queue
            int itemCount = 100;
            await AddItems(orderedQueue, itemCount);
            await AddItems(orderedQueue, itemCount, visibleAtOffsetFromNow: TimeSpan.FromHours(1));

            var watch = System.Diagnostics.Stopwatch.StartNew();
            var visibleQueueLength = await orderedQueue.ApproximateVisibleQueueLength();
            watch.Stop();
            Assert.AreEqual(itemCount, visibleQueueLength);
            //all calls are subject to network latency, so we want to be a bit lenient on expected speed timings.
            Assert.IsTrue((watch.ElapsedMilliseconds < TimeSpan.FromSeconds(0.2).TotalMilliseconds), $"ApproximateVisibleQueueLength() took {watch.ElapsedMilliseconds}, which is to slow with {itemCount} items in the queue");
        }
        [TestMethod]
        public async Task ApproximateTotalQueueLengthTest()
        {
            var table = await GetEmptyTestTable<SomeTestDataToSerialize>();
            var orderedQueue = GetTestQueue<SomeTestDataToSerialize>(table);

            //make sure we've got some items in the queue
            int itemCount = 10;
            await AddItems(orderedQueue, itemCount);

            var visibleQueueLength = await orderedQueue.ApproximateTotalQueueLength();
            Assert.AreEqual(10, visibleQueueLength);
        }

        [TestMethod]
        public async Task ApproximateTotalQueueLength2Test()
        {
            var table = await GetEmptyTestTable<SomeTestDataToSerialize>();
            var orderedQueue = GetTestQueue<SomeTestDataToSerialize>(table);

            //make sure we've got some items in the queue
            int itemCount = 5;
            await AddItems(orderedQueue, itemCount);
            await AddItems(orderedQueue, itemCount, visibleAtOffsetFromNow: TimeSpan.FromHours(1));

            var visibleQueueLength = await orderedQueue.ApproximateTotalQueueLength();
            Assert.AreEqual(10, visibleQueueLength);
        }
        [TestMethod]
        public async Task ApproximateTotalQueueLengthPerformanceTest()
        {
            var table = await GetEmptyTestTable<SomeTestDataToSerialize>();
            var orderedQueue = GetTestQueue<SomeTestDataToSerialize>(table);

            //make sure we've got some items in the queue
            int itemCount = 200;
            await AddItems(orderedQueue, itemCount);

            var watch = System.Diagnostics.Stopwatch.StartNew();
            var visibleQueueLength = await orderedQueue.ApproximateTotalQueueLength();
            watch.Stop();
            Assert.AreEqual(itemCount, visibleQueueLength);
            //all calls are subject to network latency, so we want to be a bit lenient on expected speed timings.
            Assert.IsTrue((watch.ElapsedMilliseconds < TimeSpan.FromSeconds(0.2).TotalMilliseconds), $"ApproximateTotalQueueLength() took {watch.ElapsedMilliseconds}, which is to slow with {itemCount} items in the queue");
        }

        [TestMethod]
        public async Task ApproximateTotalQueueLength2PerformanceTest()
        {
            var table = await GetEmptyTestTable<SomeTestDataToSerialize>();
            var orderedQueue = GetTestQueue<SomeTestDataToSerialize>(table);

            //make sure we've got some items in the queue
            int itemCount = 100;
            await AddItems(orderedQueue, itemCount);
            await AddItems(orderedQueue, itemCount, visibleAtOffsetFromNow: TimeSpan.FromHours(1));

            var watch = System.Diagnostics.Stopwatch.StartNew();
            var visibleQueueLength = await orderedQueue.ApproximateTotalQueueLength();
            watch.Stop();
            Assert.AreEqual((itemCount * 2), visibleQueueLength);
            //all calls are subject to network latency, so we want to be a bit lenient on expected speed timings.
            Assert.IsTrue((watch.ElapsedMilliseconds < TimeSpan.FromSeconds(0.2).TotalMilliseconds), $"ApproximateTotalQueueLength() took {watch.ElapsedMilliseconds}, which is to slow with {itemCount} items in the queue");
        }
        private async Task AddItems(OrderedQueue<SomeTestDataToSerialize> orderedQueue, int noOfItemsToAdd)
        {
            await AddItems(orderedQueue, noOfItemsToAdd, TimeSpan.Zero);
        }
        private async Task AddItems(OrderedQueue<SomeTestDataToSerialize> orderedQueue, int noOfItemsToAdd, TimeSpan visibleAtOffsetFromNow)
        {
            await AddItems(orderedQueue, noOfItemsToAdd, (i) => visibleAtOffsetFromNow);
        }
        private async Task AddItems(OrderedQueue<SomeTestDataToSerialize> orderedQueue, int noOfItemsToAdd, Func<int, TimeSpan> visibleAtOffsetFromNowFunc)
        {
            for (int i = 0; i < noOfItemsToAdd; i++)
            {
                var stringData = $"Serialised Data {DateTime.UtcNow}";
                await orderedQueue.AddItem(dataString: stringData, visibleAt: DateTime.UtcNow.Add(visibleAtOffsetFromNowFunc(i)));
            }
        }

        private async Task ModifyItemAndMerge<T>(CloudTable table, OrderedQueueEntity<T> item) where T : new()
        {
            Action<OrderedQueueEntity<T>> modificationFunc = (i) =>
            {
                i.DataJson = i.DataJson + "test";
            };
            await ModifyItemAndMerge(table, item, modificationFunc);
        }
        private async Task ModifyItemAndMerge<T>(CloudTable table, OrderedQueueEntity<T> item, Action<OrderedQueueEntity<T>> modificationFunc) where T : new()
        {
            modificationFunc(item);
            var mergeResult = await table.Merge(item);
        }
    }


    public static class Extensions
    {
        public static async Task<TableResult> Merge<T>(this CloudTable table, OrderedQueueEntity<T> item) where T : new()
        {
            var operation = TableOperation.Merge(item);
            return await table.ExecuteAsync(operation);
        }
        public static async Task AddItem(this OrderedQueue<SomeTestDataToSerialize> orderedQueue, string dataString, DateTime visibleAt)
        {
            var newSomeTestDataToSerialize = new SomeTestDataToSerialize() { TheData = dataString };
            await orderedQueue.AddItem(newSomeTestDataToSerialize, visibleAt);
        }
    }
}
