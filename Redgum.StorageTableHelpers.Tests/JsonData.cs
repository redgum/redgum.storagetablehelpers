using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.WindowsAzure.Storage.Table;
using Redgum.StorageTableHelpers.Tests.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Redgum.StorageTableHelpers.Tests
{
    [TestClass]
    public class JsonData
    {


        class TestJsonSave
        {
            public string Id { get; set; }
            public int Prop1 { get; set; }
            public string Prop2 { get; set; }
            public TestJsonSave2 Prop3 { get; set; }
        }

        class TestJsonSave2
        {
            public string Hello { get; set; }
        }

        [TestMethod]
        public async Task SaveAndReadJSON()
        {
            var r = new Random((int)(DateTime.Now.Ticks % 99999));
            var tableAccess = Utils.GetTableAccess();
            var name = "test1234create" + r.Next(999999);

            try
            {

                var t = await tableAccess.GetStorageTableClientAsync(name, true);

                var model = new TestJsonSave()
                {
                    Prop1 = 666,
                    Prop2 = "blaaaa",
                    Prop3 = new TestJsonSave2()
                    {
                        Hello = "testing 123",
                    }
                };

                await t.InsertJsonData("123456", "testing", model);

                var databack = await t.GetJsonData<TestJsonSave>("123456", "testing");

                Assert.IsTrue(databack.Prop1 == model.Prop1);
                Assert.IsTrue(databack.Prop3.Hello == model.Prop3.Hello);



            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
                throw;
            }
            finally
            {
                await tableAccess.DeleteTable(name);
            }

        }


        [TestMethod]
        public async Task JsonTable()
        {
            var r = new Random((int)(DateTime.Now.Ticks % 99999));
            var tableAccess = Utils.GetTableAccess();
            var name = "test1234create" + r.Next(999999);

            try
            {
                var client = await tableAccess.GetStorageTableClientAsync(name, true);

                var t = new SimpleTypedJsonStorageTable<TestJsonSave>("SOMEKEYTYPE", client, m => m.Id.ToString());

                var model1 = new TestJsonSave() { Id = "1", Prop3 = new TestJsonSave2() { Hello = "testing 123" } };
                var model2 = new TestJsonSave() { Id = "2", Prop3 = new TestJsonSave2() { Hello = "testing abcd" } };

                await t.Store(model1);
                await t.Store(model2);

                var back = await t.Get(model1.Id);
                Assert.IsTrue(model1.Prop3.Hello == back.Prop3.Hello);

                var all = await t.List();
                Assert.IsTrue(all.Count == 2);

                // do an upsert.. see if that works
                model1.Prop1 = 34534534;
                await t.Store(model1);

                var back2 = await t.Get(model1.Id);
                Assert.IsTrue(model1.Prop1 == back2.Prop1);

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
                throw;
            }
            finally
            {
                await tableAccess.DeleteTable(name);
            }
        }

        [TestMethod]
        public async Task BatchJsonTable()
        {
            var r = new Random((int)(DateTime.Now.Ticks % 99999));
            var tableAccess = Utils.GetTableAccess();
            var name = "test1234create" + r.Next(999999);

            try
            {
                var client = await tableAccess.GetStorageTableClientAsync(name, true);

                var t = new SimpleTypedJsonStorageTable<TestJsonSave>("SOMEKEYTYPE", client, m => m.Id.ToString());

                var model1 = new TestJsonSave() { Id = "1", Prop3 = new TestJsonSave2() { Hello = "testing 123" } };
                var model2 = new TestJsonSave() { Id = "2", Prop3 = new TestJsonSave2() { Hello = "testing abcd" } };
                var models = new List<TestJsonSave>();
                models.Add(model1);
                models.Add(model2);

                await t.StoreBatch(models);

                var back = await t.List();
                Assert.IsTrue(back.Count == 2);
                Assert.IsTrue(model1.Prop3.Hello == back[0].Prop3.Hello);
                Assert.IsTrue(model2.Prop3.Hello == back[1].Prop3.Hello);

                // add another the same list with one extra and a change
                model1.Prop3.Hello = "zzzzzzzzzzzzzzzzzzz";
                var model3 = new TestJsonSave() { Id = "3", Prop3 = new TestJsonSave2() { Hello = "213345657457676" } };
                models.Add(model3);

                await t.StoreBatch(models);
                back = await t.List();
                Assert.IsTrue(back.Count == 3);
                Assert.IsTrue(model1.Prop3.Hello == back[0].Prop3.Hello);
                Assert.IsTrue(model2.Prop3.Hello == back[1].Prop3.Hello);
                Assert.IsTrue(model3.Prop3.Hello == back[2].Prop3.Hello);

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
                throw;
            }
            finally
            {
                await tableAccess.DeleteTable(name);
            }
        }
    }
}
