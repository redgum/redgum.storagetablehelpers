﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.WindowsAzure.Storage.Table;
using Moq;
using Redgum.StorageTableHelpers.Tests.Helpers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Redgum.StorageTableHelpers.Tests
{
    [TestClass]
    public class SimpleTypedJsonStorageTableTests
    {

        [TestMethod]
        public void TestContructor()
        {
            var keyType = "contructorTestKey";
            var storageClient = new Mock<IStorageTableClient>();

            Func<int, string> getKeyValueFunc = (theInt) => { return theInt.ToString(); };
            var simpleTypedJsonStorageTable = new SimpleTypedJsonStorageTable<int>(keyType, storageClient.Object, getKeyValueFunc);
            Assert.IsNotNull(simpleTypedJsonStorageTable);
        }

        [TestMethod]
        public async Task GetInt()
        {
            var masterPartitionKey = "int";
            var masterRowId = "TheGetKeyValueFuncResult";
            Func<int, string> getKeyValueFunc = (theInt) => { return masterRowId; };
            var masterItem = 1;

            var storageClient = new Mock<IStorageTableClient>();
            storageClient
                .Setup(
                    x => x.GetJsonData<int>(
                        It.Is<string>(partitionKey => partitionKey == masterPartitionKey),
                        It.Is<string>(rowKey => rowKey == masterRowId)
                        )
                    )
                .Returns(Task.FromResult(masterItem))
                ;
            var simpleTypedJsonStorageTable = new SimpleTypedJsonStorageTable<int>(masterPartitionKey, storageClient.Object, getKeyValueFunc);
            var getResult = await simpleTypedJsonStorageTable.Get(masterRowId);
            Assert.AreEqual(masterItem, getResult);
        }

        [TestMethod]
        public async Task StoreInt()
        {
            var masterPartitionKey = "int";
            var masterRowId = "TheGetKeyValueFuncResult";
            Func<int, string> getKeyValueFunc = (theInt) => { return masterRowId; };
            var masterItem = 1;

            var storageClient = new Mock<IStorageTableClient>();
            storageClient.Setup(
                x => x.UpsertJsonData(
                    It.Is<string>(partitionKey => partitionKey == masterPartitionKey),
                    It.Is<string>(keyValue => keyValue == masterRowId),
                    It.Is<int>(obj => obj == masterItem)
                    )
                )
                .Returns(Task.CompletedTask)
                ;
            var simpleTypedJsonStorageTable = new SimpleTypedJsonStorageTable<int>(masterPartitionKey, storageClient.Object, getKeyValueFunc);
            Assert.IsNotNull(simpleTypedJsonStorageTable);
            await simpleTypedJsonStorageTable.Store(masterItem);
        }
        [TestMethod]
        public async Task DeleteInt()
        {
            var masterPartitionKey = "int";
            var masterRowId = "TheGetKeyValueFuncResult";
            Func<int, string> getKeyValueFunc = (theInt) => { return masterRowId; };

            var storageClient = new Mock<IStorageTableClient>();
            storageClient
                .Setup(
                    x => x.Delete(
                        It.Is<string>(partitionKey => partitionKey == masterPartitionKey),
                        It.Is<string>(rowKey => rowKey == masterRowId)
                        )
                    )
                .ReturnsAsync(new TableResult())
                ;
            var simpleTypedJsonStorageTable = new SimpleTypedJsonStorageTable<int>(masterPartitionKey, storageClient.Object, getKeyValueFunc);
            await simpleTypedJsonStorageTable.Delete(masterRowId);
        }

        [TestMethod]
        public async Task ListInt()
        {
            var masterPartitionKey = "int";
            var masterRowId = "TheGetKeyValueFuncResult";
            Func<int, string> getKeyValueFunc = (theInt) => { return masterRowId; };
            var masterItems = new List<int>() { 1, 2, 3 };
            var readOnlyMasterItems = (IReadOnlyList<int>)masterItems;

            var storageClient = new Mock<IStorageTableClient>();
            storageClient
                .Setup(
                    x => x.GetJsonData<int>(
                        It.Is<string>(partitionKey => partitionKey == masterPartitionKey)
                        )
                    )
                .Returns(Task.FromResult(readOnlyMasterItems))
                ;
            var simpleTypedJsonStorageTable = new SimpleTypedJsonStorageTable<int>(masterPartitionKey, storageClient.Object, getKeyValueFunc);
            var getResult = await simpleTypedJsonStorageTable.List();
            Assert.AreSame(readOnlyMasterItems, getResult);
        }

        [TestMethod]
        public async Task GetString()
        {
            var masterPartitionKey = "string";
            var masterRowId = "TheGetKeyValueFuncResult";
            Func<string, string> getKeyValueFunc = (theString) => { return masterRowId; };
            var masterItem = "Item";

            var storageClient = new Mock<IStorageTableClient>();
            storageClient
                .Setup(
                    x => x.GetJsonData<string>(
                        It.Is<string>(partitionKey => partitionKey == masterPartitionKey),
                        It.Is<string>(rowKey => rowKey == masterRowId)
                        )
                    )
                .Returns(Task.FromResult(masterItem))
                ;
            var simpleTypedJsonStorageTable = new SimpleTypedJsonStorageTable<string>(masterPartitionKey, storageClient.Object, getKeyValueFunc);
            var getResult = await simpleTypedJsonStorageTable.Get(masterRowId);
            Assert.AreEqual(masterItem, getResult);
        }

        [TestMethod]
        public async Task StoreString()
        {
            var masterPartitionKey = "string";
            var masterRowId = "TheGetKeyValueFuncResult";
            Func<string, string> getKeyValueFunc = (theString) => { return masterRowId; };
            var masterItem = "Item";

            var storageClient = new Mock<IStorageTableClient>();
            storageClient.Setup(
                x => x.UpsertJsonData(
                    It.Is<string>(partitionKey => partitionKey == masterPartitionKey),
                    It.Is<string>(keyValue => keyValue == masterRowId),
                    It.Is<string>(obj => obj == masterItem)
                    )
                )
                .Returns(Task.CompletedTask)
                ;
            var simpleTypedJsonStorageTable = new SimpleTypedJsonStorageTable<string>(masterPartitionKey, storageClient.Object, getKeyValueFunc);
            Assert.IsNotNull(simpleTypedJsonStorageTable);
            await simpleTypedJsonStorageTable.Store(masterItem);
        }
        [TestMethod]
        public async Task DeleteString()
        {
            var masterPartitionKey = "string";
            var masterRowId = "TheGetKeyValueFuncResult";
            Func<string, string> getKeyValueFunc = (theString) => { return masterRowId; };

            var storageClient = new Mock<IStorageTableClient>();
            storageClient
                .Setup(
                    x => x.Delete(
                        It.Is<string>(partitionKey => partitionKey == masterPartitionKey),
                        It.Is<string>(rowKey => rowKey == masterRowId)
                        )
                    )
                .ReturnsAsync(new TableResult())
                ;
            var simpleTypedJsonStorageTable = new SimpleTypedJsonStorageTable<string>(masterPartitionKey, storageClient.Object, getKeyValueFunc);
            await simpleTypedJsonStorageTable.Delete(masterRowId);
        }

        [TestMethod]
        public async Task ListString()
        {
            var masterPartitionKey = "string";
            var masterRowId = "TheGetKeyValueFuncResult";
            Func<string, string> getKeyValueFunc = (theString) => { return masterRowId; };
            var masterItems = new List<string>() { "StringOne", "and", "StringTwo" }; //these things will not bite you, they want to have fun
            var readOnlyMasterItems = (IReadOnlyList<string>)masterItems;

            var storageClient = new Mock<IStorageTableClient>();
            storageClient
                .Setup(
                    x => x.GetJsonData<string>(
                        It.Is<string>(partitionKey => partitionKey == masterPartitionKey)
                        )
                    )
                .Returns(Task.FromResult(readOnlyMasterItems))
                ;
            var simpleTypedJsonStorageTable = new SimpleTypedJsonStorageTable<string>(masterPartitionKey, storageClient.Object, getKeyValueFunc);
            var getResult = await simpleTypedJsonStorageTable.List();
            Assert.AreSame(readOnlyMasterItems, getResult);
        }

        [TestMethod]
        public async Task GetObject()
        {
            var masterPartitionKey = "TestClassPartitionKey";
            var masterRowId = "TheGetKeyValueFuncResult";
            Func<TestClass, string> getKeyValueFunc = (theObject) => { return masterRowId; };
            var masterItem = GetTestClass();

            var storageClient = new Mock<IStorageTableClient>();
            storageClient
                .Setup(
                    x => x.GetJsonData<TestClass>(
                        It.Is<string>(partitionKey => partitionKey == masterPartitionKey),
                        It.Is<string>(rowKey => rowKey == masterRowId)
                        )
                    )
                .Returns(Task.FromResult(masterItem))
                ;
            var simpleTypedJsonStorageTable = new SimpleTypedJsonStorageTable<TestClass>(masterPartitionKey, storageClient.Object, getKeyValueFunc);
            var getResult = await simpleTypedJsonStorageTable.Get(masterRowId);
            Assert.AreSame(masterItem, getResult);
        }

        [TestMethod]
        public async Task StoreObject()
        {
            var masterPartitionKey = "TestClassPartitionKey";
            var masterRowId = "TheGetKeyValueFuncResult";
            Func<TestClass, string> getKeyValueFunc = (theObject) => { return masterRowId; };
            var masterItem = GetTestClass();

            var storageClient = new Mock<IStorageTableClient>();
            storageClient.Setup(
                x => x.UpsertJsonData(
                    It.Is<string>(partitionKey => partitionKey == masterPartitionKey),
                    It.Is<string>(keyValue => keyValue == masterRowId),
                    It.Is<TestClass>(obj => obj == masterItem)
                    )
                )
                .Returns(Task.CompletedTask)
                ;
            var simpleTypedJsonStorageTable = new SimpleTypedJsonStorageTable<TestClass>(masterPartitionKey, storageClient.Object, getKeyValueFunc);
            Assert.IsNotNull(simpleTypedJsonStorageTable);
            await simpleTypedJsonStorageTable.Store(masterItem);
        }
        [TestMethod]
        public async Task DeleteObject()
        {
            var masterPartitionKey = "TestClassPartitionKey";
            var masterRowId = "TheGetKeyValueFuncResult";
            Func<TestClass, string> getKeyValueFunc = (theObject) => { return masterRowId; };
            var masterItem = GetTestClass();

            var storageClient = new Mock<IStorageTableClient>();
            storageClient
                .Setup(
                    x => x.Delete(
                        It.Is<string>(partitionKey => partitionKey == masterPartitionKey),
                        It.Is<string>(rowKey => rowKey == masterRowId)
                        )
                    )
                .ReturnsAsync(new TableResult())
                ;
            var simpleTypedJsonStorageTable = new SimpleTypedJsonStorageTable<TestClass>(masterPartitionKey, storageClient.Object, getKeyValueFunc);
            await simpleTypedJsonStorageTable.Delete(masterRowId);
        }

        [TestMethod]
        public async Task ListObject()
        {
            var masterPartitionKey = "TestClassPartitionKey";
            var masterRowId = "TheGetKeyValueFuncResult";
            Func<TestClass, string> getKeyValueFunc = (theObject) => { return masterRowId; };
            var masterItems = new List<TestClass>() { GetTestClass(), GetTestClass(), GetTestClass() }; //these things will not bite you, they want to have fun
            var readOnlyMasterItems = (IReadOnlyList<TestClass>)masterItems;

            var storageClient = new Mock<IStorageTableClient>();
            storageClient
                .Setup(
                    x => x.GetJsonData<TestClass>(
                        It.Is<string>(partitionKey => partitionKey == masterPartitionKey)
                        )
                    )
                .Returns(Task.FromResult(readOnlyMasterItems))
                ;
            var simpleTypedJsonStorageTable = new SimpleTypedJsonStorageTable<TestClass>(masterPartitionKey, storageClient.Object, getKeyValueFunc);
            var getResult = await simpleTypedJsonStorageTable.List();
            Assert.AreSame(readOnlyMasterItems, getResult);
        }

        [TestMethod]
        public async Task JSONTableEverything()
        {
            // test the json table thingy with a real db.. test all functions in one go

            var tableAccess = Utils.GetTableAccess();

            var r = new Random((int)(DateTime.Now.Ticks % 99999));
            var name = "test1234jsontester" + r.Next(999999);

            var table = new SimpleTypedJsonStorageTable<TestClass>("SOMEKEY", tableAccess.GetStorageTableClient(name, true), id => id.StringOne);

            var data1 = new TestClass()
            {
                IntOne = 1,
                IntTwo = 2,
                StringOne = "dfgsdfg",
                StringTwo = "zzzzz",
            };


            var data2 = new TestClass()
            {
                IntOne = 6,
                IntTwo = 6,
                StringOne = "777",
                StringTwo = "888",
            };

            await table.Store(data1);
            await table.Store(data2);

            // does it load back ok?
            var back = await table.Get(data1.StringOne);
            Assert.IsTrue(back.IntOne == data1.IntOne);

            // does it load back ok as a list?
            var listy = await table.List();
            Assert.IsTrue(listy.Count==2);

            // does it exists?
            var exists = await table.Exists(data1.StringOne);
            Assert.IsTrue(exists);

            // we can delete?
            await table.Delete(data1.StringOne);
            var deleted = await table.Get(data1.StringOne);
            Assert.IsTrue(deleted==null);

            // is it gone?
            var stillThere= await table.Exists(data1.StringOne);
            Assert.IsTrue(!stillThere);

            // clean up
            await tableAccess.DeleteTable(name);
        }


        public static TestClass GetTestClass()
        {
            return new TestClass()
            {
                IntOne = 1,
                IntTwo = 2,
                StringOne = "One",
                StringTwo = "Two"
            };
        }

        public class TestClass
        {
            public int IntOne { get; set; }
            public int IntTwo { get; set; }
            public string StringOne { get; set; }
            public string StringTwo { get; set; }
        }
    }
}
