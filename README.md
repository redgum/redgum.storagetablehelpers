# Redgum Helpers for Azure Tables Storage 


_INTERNAL NOTE:
Do not store any passwords or connection strings in this project, this is a public repo_

To set the config for the table storage connection

```
  "Azure": {
    "StorageConnection": "connection goes here"
  },
```

then in startup.cs

```
services.Configure<AzureSettings>(Configuration.GetSection("Azure"));
services.AddStorageTableHelpers();
```


The Redgum.StorageTableHelpers is built using Bitbucket Pipelines there is a yml file that defines how this works.

## dotnet core 2.0 support
Master branch targets dotnet core 2.2. If you need 2.0 support, there's a core2.0 branch. If you're making changes, try and keep feature compatibility between the branches where possible

